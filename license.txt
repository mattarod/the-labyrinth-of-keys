Source code and most assets (C) 2014 Matthew A. Rodriguez
Music by Adan Ferguson
Levels and some assets by ZeroTron

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.

