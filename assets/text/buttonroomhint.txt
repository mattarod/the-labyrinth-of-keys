Didst thou guess wrong?
The magic of the key maintaineth this room.
When thou pickest it up, it causeth many
rooms in this labyrinth to reset.
Warp back to the start, take it, and return.
