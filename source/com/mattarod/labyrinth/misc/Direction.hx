package com.mattarod.labyrinth.misc;

/**
 * An enum representing the four cardinal dimensions in a 2D side view.
 * 
 * @author Matthew Rodriguez
 */
enum Direction {
	UP;
	DOWN;
	LEFT;
	RIGHT;
}