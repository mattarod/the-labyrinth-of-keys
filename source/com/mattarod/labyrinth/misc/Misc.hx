package com.mattarod.labyrinth.misc;

/**
 * A class containing miscellaneous general functions.
 *
 * @author Matthew Rodriguez
 */
class Misc {

	/**
	 * Helper function that converts a string of comma-separated boolean values
	 * into an array of boolean values.
	 * @param	bs a string of comma-separated booleans
	 * @return An array of booleans.
	 */
	public static function parseBoolArray(bs:Array<String>):Array<Bool> {
		
		var result:Array<Bool> = new Array<Bool>();
		
		for(b in bs) {
			result.push(switch(b) {
				case "true": true;
				case "false": false;
				default: throw "invalid boolean value: " + b;
			});
		}
		
		return result;
	}
	
	/**
	 * Finds the ceiling of the binary log of n.
	 * @param	n
	 * @return The ceiling of the binary log of n.
	 */
	public static function binaryLogCeiling(n:Int):Int {
		
		for (i in 0...31) {
			if (Math.pow(2, i) >= n) {
				return i;
			}
		}
		
		return -1;
	}
}
