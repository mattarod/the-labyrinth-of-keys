package com.mattarod.labyrinth;

/**
 * A class that contains the game's configuration.
 * Contains the player's keybinds.
 * 
 * @author Matthew Rodriguez
 */
class Config {
	public static var moveLeft:String = "LEFT";
	public static var moveRight:String = "RIGHT";
	public static var jump:String = "X";
	public static var useKey:String = "K";
	public static var warp:String = "W";
	public static var reset:String = "R";
	public static var pause:String = "P";
	public static var muteMusic:String = "M";
}
