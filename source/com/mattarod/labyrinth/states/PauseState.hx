package com.mattarod.labyrinth.states;

import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;

/**
 * A FlxSubState that shows a sprite that says "PAUSE".
 * Used for pausing the game.
 * 
 * @author Matthew Rodriguez
 */
class PauseState extends FlxSubState {

	public override function create():Void {
		var pauseWindow:FlxSprite = new FlxSprite();
		pauseWindow.loadGraphic("assets/png/pause.png");
		pauseWindow.x = (KagiNoMeiro.GAME_WIDTH - pauseWindow.width) / 2 + Registry.gameState.room.x;
		pauseWindow.y = (KagiNoMeiro.GAME_HEIGHT - pauseWindow.height) / 2 + Registry.gameState.room.y;
		
		add(pauseWindow);
		
		FlxG.sound.music.pause();
		FlxG.sound.destroySounds();
		FlxG.sound.play("Pause");
	}
	
	public override function update():Void {
		if (FlxG.keyboard.justPressed(Config.pause)) {
			FlxG.sound.music.play();
			close();
		}
	}
	
}
