package com.mattarod.labyrinth.states;

import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;

/**
 * A FlxState that displays the ending screen.
 */
class VictoryScreen extends FlxState {

	/**
	 * Initialize the items on the victory screen.
	 */
	public override function create():Void {
		Registry.game.readjustCamera();
		
		// Set a background color
		FlxG.cameras.bgColor = 0xFF000000;
		
		// Add "Congratulations" in large letters
		var congratulations:FlxText = new FlxText(0, 16, 320, "CONGRATULATIONS!");
		congratulations.setFormat("assets/font/bitrod.ttf", 16, 0xFFFFFF, "center");
		add(congratulations);
		
		// Add the credits
		var credits:FlxText = new FlxText(0, 48, 320,
			"You have escaped the Labyrinth of Keys!\n" +
			"Thank you for playing!\n\n" +
			
			"Credits:\n" +
			"Matthew Rodriguez\n" +
			"ZeroTron\n" +
			"Adan Ferguson\n\n" +
			
			"Special Thanks:\n" +
			"Alex\n" +
			"NIGORO");
			
		credits.setFormat("assets/font/bitrod.ttf", 8, 0xFFFFFF, "center");
		add(credits);
		
		// Play the ending music
		FlxG.sound.playMusic("Victory", KagiNoMeiro.MUSIC_VOLUME);
	}
}
