package com.mattarod.labyrinth.states;

import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import openfl.Assets;

/**
 * A FlxState that displays the title screen,
 * and transitions to the main GameState.
 */
class TitleScreen extends FlxState {
	
	/**
	 * Initialize the items on the title screen.
	 */
	public override function create():Void {
		Registry.game.readjustCamera();
		
		// Set the background color to black.
		FlxG.cameras.bgColor = 0xFF000000;
		
		// Add the title in Japanese kanji.
		var kanji:FlxSprite = new FlxSprite(64, 16, Assets.getBitmapData("assets/png/kanji.png"));
		add(kanji);
		
		// Add the title in Japanese romaji and English.
		var title:FlxText = new FlxText(0, 80, 320, "Kagi no Meiro\nThe Labyrinth of Keys");
		title.setFormat("assets/font/bitrod.ttf", 16, 0xFFFFFF, "center");
		add(title);
		
		// Add the directions.
		var sub:FlxText = new FlxText(0, 144, 320, "Press any key to begin.");
		sub.setFormat("assets/font/bitrod.ttf", 8, 0xFFFFFF, "center");
		add(sub);
		
		// Play the game's BGM.
		FlxG.sound.playMusic("Keyorama", KagiNoMeiro.MUSIC_VOLUME);
	}

	/**
	 * Update function.
	 */
	public override function update():Void {
		super.update();
		
		// Transition to GameState when the player presses any key.
		if (FlxG.keys.justPressed.ANY) {
			FlxG.switchState(new GameState());
		}
	}
}
