package com.mattarod.labyrinth.states;

import com.mattarod.labyrinth.Config;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.gameobjects.Player;
import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.misc.Direction;
import com.mattarod.labyrinth.Registry;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxText;
import flixel.util.FlxPoint;
import openfl.Assets;

/**
 * The FlxState in which all of the game's action takes place.
 */
class GameState extends FlxState {
	
	/**
	 * How long a screen transition takes in seconds
	 */
	public static inline var TRANSITION_PERIOD:Float = 0.5;
	
	private var _room:Room;
	private var oldRoom:Room;
	private var roomGroup:FlxGroup;
	private var solids:FlxGroup;
	
	private var _player:Player;
	private var bookText:FlxText;
	private var bookWindow:FlxSprite;
	private var lockStealingKey:FlxSprite;
	
	private var transitionTime:Float;
	private var transitionCamera:FlxCamera;
	private var transitionDirection:Direction;
	private var transitionPlayerSprite:FlxSprite;
	
	public override function create():Void {
		Registry.game.readjustCamera();
		
		Registry.gameState = this;
		
		solids = new FlxGroup();
		roomGroup = new FlxGroup();
		
		add(roomGroup);
		
		// Load the start room
		_room = Registry.startRoom;
		_room.paused = false;
		roomGroup.add(_room);
		solids.add(_room.solids);
		
		FlxG.camera.focusOn(new FlxPoint(_room.x + KagiNoMeiro.GAME_WIDTH / 2, _room.y + KagiNoMeiro.GAME_HEIGHT / 2));
		FlxG.worldBounds.set(_room.x, _room.y, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT);
		
		// Add the player
		_player = new Player();
		add(_player.sideCollisionBox);
		add(_player);
		
		// Add the text window for books, and make it invisible to start.
		bookWindow = new FlxSprite(32, 0);
		bookWindow.loadGraphic(Assets.getBitmapData("assets/png/textwindow.png"));
		bookWindow.visible = false;
		add(bookWindow);
		
		bookText = new FlxText(38, 0, 244, "");
		bookText.setFormat("assets/font/bitrod.ttf");
		bookText.visible = false;
		add(bookText);
		
		// Add a sprite for playing the "lock stealing key" animation.
		// Invisible to start.
		lockStealingKey = new FlxSprite(0, 0, "assets/png/lockwithkey.png");
		lockStealingKey.exists = false;
		lockStealingKey.velocity.x = -128;
		lockStealingKey.velocity.y = -96;
		lockStealingKey.immovable = true;
		add(lockStealingKey);
		
		transitionTime = 0.0;
	}

	/**
	 * Update function.
	 */
	public override function update():Void {
		// If the state is currently undergoing a screen transition, handle it.
		if (transitionTime > 0) {
			
			// Decrement the transitionTime control variable.
			transitionTime -= FlxG.elapsed;
			
			if (transitionTime > 0) {
				// Countdown of how far along the transition is.
				// 1 is "just started."
				// 0 is "completed."
				var transitionRatio:Float = transitionTime / TRANSITION_PERIOD;
				
				// What follows is an explanation for this complicated bit of code.
				// FlxG.camera is a view of the new room.
				// transitionCamera is a view of the old room.
				// transitionCamera is pushed out of the frame in the opposite direction of the transition,
				// and FlxG.camera comes into the frame from the direction of the transition.
				// If the screen is transitioning to the right, for example, transitionCamera is pushed out
				// of the frame to the left, and FlxG.camera comes into the frame from the right.
				// The transitionRatio variable is used to determine
				// the cameras' bounds, positions, and dimensions.
				// This complicated two-camera approach is necessitated by the fact that the dungeon wraps
				// in both dimensions.
				// A simple one-camera solution would work of a normal transition.
				
				switch(transitionDirection) {
					case RIGHT:
						var transitionWidth:Int = Math.round(transitionRatio * KagiNoMeiro.GAME_WIDTH);
						var mainCameraWidth:Int = KagiNoMeiro.GAME_WIDTH - transitionWidth;
						
						FlxG.camera.setBounds(room.x, room.y, mainCameraWidth, KagiNoMeiro.GAME_HEIGHT);
						FlxG.camera.x = Registry.game.xOffset + transitionWidth * Registry.game.ratio;
						FlxG.camera.width = mainCameraWidth;
						
						transitionCamera.setBounds(oldRoom.x + KagiNoMeiro.GAME_WIDTH - transitionWidth,
							oldRoom.y, transitionWidth, KagiNoMeiro.GAME_HEIGHT);
						transitionCamera.x = Registry.game.xOffset;
						transitionCamera.width = transitionWidth;
						
					case LEFT:
						var transitionWidth:Int = Math.round(transitionRatio * KagiNoMeiro.GAME_WIDTH);
						var mainCameraWidth:Int = KagiNoMeiro.GAME_WIDTH - transitionWidth;
						
						FlxG.camera.setBounds(room.x + transitionWidth, room.y, mainCameraWidth, KagiNoMeiro.GAME_HEIGHT);
						FlxG.camera.x = Registry.game.xOffset;
						FlxG.camera.width = mainCameraWidth;
						
						transitionCamera.setBounds(oldRoom.x, oldRoom.y, transitionWidth, KagiNoMeiro.GAME_HEIGHT);
						transitionCamera.x = Registry.game.xOffset + mainCameraWidth * Registry.game.ratio;
						transitionCamera.width = transitionWidth;
						
					case DOWN:
						var transitionHeight:Int = Math.round(transitionRatio * KagiNoMeiro.GAME_HEIGHT);
						var mainCameraHeight:Int = KagiNoMeiro.GAME_HEIGHT - transitionHeight;
						
						FlxG.camera.setBounds(room.x, room.y, KagiNoMeiro.GAME_WIDTH, mainCameraHeight);
						FlxG.camera.y = Registry.game.yOffset + transitionHeight * Registry.game.ratio;
						FlxG.camera.height = mainCameraHeight;
						
						transitionCamera.setBounds(oldRoom.x, oldRoom.y + KagiNoMeiro.GAME_HEIGHT - transitionHeight,
							KagiNoMeiro.GAME_WIDTH, transitionHeight);
						transitionCamera.y = Registry.game.yOffset;
						transitionCamera.height = transitionHeight;
						
					case UP:
						var transitionHeight:Int = Math.round(transitionRatio * KagiNoMeiro.GAME_HEIGHT);
						var mainCameraHeight:Int = KagiNoMeiro.GAME_HEIGHT - transitionHeight;
						
						FlxG.camera.setBounds(room.x, room.y + transitionHeight, KagiNoMeiro.GAME_WIDTH, mainCameraHeight);
						FlxG.camera.y = Registry.game.yOffset;
						FlxG.camera.height = mainCameraHeight;
						
						transitionCamera.setBounds(oldRoom.x, oldRoom.y, KagiNoMeiro.GAME_WIDTH, transitionHeight);
						transitionCamera.y = Registry.game.yOffset + mainCameraHeight * Registry.game.ratio;
						transitionCamera.height = transitionHeight;
				}
				
				// Do nothing except update the cameras.
				return;
				
			} else {
				// Once the transition is over, set FlxG.camera to normal values.
				
				FlxG.camera.x = Registry.game.xOffset;
				FlxG.camera.y = Registry.game.yOffset;
				FlxG.camera.width = KagiNoMeiro.GAME_WIDTH;
				FlxG.camera.height = KagiNoMeiro.GAME_HEIGHT;
				FlxG.camera.setBounds(room.x, room.y, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT);
				
				// Remove the transition camera, the old room, and the transitionPlayerSprite.
				FlxG.cameras.remove(transitionCamera);
				roomGroup.remove(oldRoom);
				remove(transitionPlayerSprite);
			}
		}
		
		// Calls update() on all objects added to the state.
		super.update();
		
		// Reset the game if the player presses the Reset button.
		if (FlxG.keyboard.justPressed(Config.reset)) {
			FlxG.resetGame();
			Registry.game.setUp();
		}
		
		// Mute or unmute the music if the player presses the Mute Music button.
		if (FlxG.keyboard.justPressed(Config.muteMusic)) {
			if(FlxG.sound.music.volume == 0) {
				FlxG.sound.music.volume = KagiNoMeiro.MUSIC_VOLUME;
			} else {
				FlxG.sound.music.volume = 0;
			}
		}
		
		// Pause the game if the player presses the Pause button.
		if (FlxG.keyboard.justPressed(Config.pause)) {
			setSubState(new PauseState());
		}
		
		// Warp if the player presses the Warp button.
		if (FlxG.keyboard.justPressed(Config.warp)) {
			if (warp()) {
				return;
			}
		}
		
		// If the player is crushed...
		if (_player.crushed) {
			// If he's fallen off the screen, warp back to the start.
			if (_player.y > _room.y + KagiNoMeiro.GAME_HEIGHT) {
				warp();
			}
			
			return;
		}
		
		// Collide the player's sideCollisionBox with solids.
		// The sideCollisionBox is a workaround to an unwanted behavior in Flixel.
		// If there is a wall consisting of Tilemap blocks and FlxSprite blocks,
		// and the player jumps into that wall,
		// the player will falsely "land" on some of the blocks,
		// interrupting his momentum.
		// By creating an object that is identical to the player,
		// that only collides with objects on either side,
		// we can avoid such false landings.
		FlxG.collide(_player.sideCollisionBox, solids);
		
		// Set the player's x-position to the sideCollisionBox's x-position.
		_player.setPosition(_player.sideCollisionBox.x, _player.y);
		
		// Collide the player with solids.
		FlxG.collide(_player, solids);
		
		// Align the sideCollisionBox again.
		_player.sideCollisionBox.setPosition(_player.x, _player.y);
		
		// Crush the player if he's being touched on both sides
		// or top and bottom.
		if ((_player.isTouching(FlxObject.LEFT) && _player.isTouching(FlxObject.RIGHT)) ||
			(_player.isTouching(FlxObject.UP) && _player.isTouching(FlxObject.DOWN))) {
			
			_player.crush();
			return;
		}
		
		// Check for overlaps between the player and FlxInteractiveSprites
		// in the room, and handle them.
		FlxG.overlap(_player, _room, handlePlayerRoomCollision);
		
		// Handle screen transitions.
		
		// Screen transition right
		if (_player.getMidpoint().x > _room.x + KagiNoMeiro.GAME_WIDTH) {
			transitionTime = TRANSITION_PERIOD;
			transitionDirection = Direction.RIGHT;
			
			loadRoom(Registry.rooms[(_room.xIndex + Registry.roomsWide + 1) % Registry.roomsWide][_room.yIndex]);
			
			if (_room.xIndex == 0) {
				createTransitionPlayerSprite();
				_player.setPosition(_player.x - (KagiNoMeiro.GAME_WIDTH * Registry.roomsWide), _player.y);
			}
		}
		
		// Screen transition left
		else if (_player.getMidpoint().x < _room.x) {
			transitionTime = TRANSITION_PERIOD;
			transitionDirection = Direction.LEFT;
			
			loadRoom(Registry.rooms[(_room.xIndex + Registry.roomsWide - 1) % Registry.roomsWide][_room.yIndex]);
			
			if (_room.xIndex == Registry.roomsWide - 1) {
				createTransitionPlayerSprite();
				_player.setPosition(_player.x + (KagiNoMeiro.GAME_WIDTH * Registry.roomsWide), _player.y);
			}
		}
		
		// Screen transition down
		if (_player.getMidpoint().y > _room.y + KagiNoMeiro.GAME_HEIGHT) {
			transitionTime = TRANSITION_PERIOD;
			transitionDirection = Direction.DOWN;
			
			loadRoom(Registry.rooms[_room.xIndex][(_room.yIndex + Registry.roomsTall + 1) % Registry.roomsTall]);
			
			if (_room.yIndex == 0) {
				createTransitionPlayerSprite();
				_player.setPosition(_player.x, _player.y - (KagiNoMeiro.GAME_HEIGHT * Registry.roomsTall));
			}
		}
		
		// Screen transition up
		else if (_player.getMidpoint().y < _room.y) {
			transitionTime = TRANSITION_PERIOD;
			transitionDirection = Direction.UP;
			
			loadRoom(Registry.rooms[_room.xIndex][(_room.yIndex + Registry.roomsTall - 1) % Registry.roomsTall]);
			
			if (_room.yIndex == Registry.roomsTall - 1) {
				createTransitionPlayerSprite();
				_player.setPosition(_player.x, _player.y + (KagiNoMeiro.GAME_HEIGHT * Registry.roomsTall));
			}
		}
	}
	
	/**
	 * Create a sprite that looks identical to the player's current frame
	 * in the exact same position.
	 * Used for screen transitions that wrap around the dungeon.
	 */
	private function createTransitionPlayerSprite() {
		transitionPlayerSprite = new FlxSprite(_player.x, _player.y);
		transitionPlayerSprite.loadfromSprite(_player);
		transitionPlayerSprite.facing = _player.facing;
		transitionPlayerSprite.offset = _player.offset;
		add(transitionPlayerSprite);
	}
	
	/**
	 * Display the given text on the screen.
	 *
	 * @param	text The text to display.
	 */
	public function openBook(text:String):Void {
		// Show the text window.
		bookText.visible = true;
		bookWindow.visible = true;
		
		// Replace Windows-style newlines with Unix-style,
		// Because FlxText interprets \r\n as two newlines
		bookText.text = StringTools.replace(text, "\r\n", "\n");
		
		// Put the window on the top of the screen.
		if (_player.getMidpoint().y > KagiNoMeiro.GAME_HEIGHT / 2) {
			bookWindow.setPosition(32 + _room.x, _room.y + 32);
			bookText.setPosition(38 + _room.x, _room.y + 37);
			
		// Put the window on the bottom of the screen.
		} else {
			bookWindow.setPosition(32 + _room.x, _room.y + 144);
			bookText.setPosition(38 + _room.x, _room.y + 149);
		}
	}
	
	/**
	 * Hide the text window upon a book closing.
	 */
	public function closeBook():Void {
		bookText.visible = false;
		bookWindow.visible = false;
	}
	
	/**
	 * Handle collisions between the player and FlxInteractiveSprites.
	 *
	 * @param	player The player sprite.
	 * @param	roomObject The object the player collided with.
	 */
	private function handlePlayerRoomCollision(player:Dynamic, roomObject:Dynamic):Void {
		// If the object is not an FlxInteractiveSprite, do nothing.
		if (!Std.is(roomObject, FlxInteractiveSprite)) {
			return;
		}
		
		// Call the FlxInteractiveSprite's onPlayerCollision() method.
		var interactiveRoomObject:FlxInteractiveSprite = cast roomObject;
		
		interactiveRoomObject.onPlayerCollision();
	}
	
	/**
	 * Try to warp back to the start room.
	 * Fail if in start room already.
	 * 
	 * @param force Forces the warp if the player is crushed in the start room.
	 * 
	 * @return true if warped, false otherwise
	 */
	public function warp():Bool {
		// Do nothing if in start room already.
		if (!player.crushed && _room == Registry.startRoom) {
			return false;
		}
		
		// Load the start room.
		loadRoom(Registry.startRoom);
		
		_player.resetOnWarp();
		
		return true;
	}
	
	/**
	 * Helper function that loads a room.
	 * 
	 * @param	newRoom The room to load.
	 */
	private function loadRoom(newRoom:Room):Void {
		// Get rid of the lockStealingKey sprite.
		lockStealingKey.exists = false;
		
		// Pause the old room.
		oldRoom = _room;
		oldRoom.paused = true;
		
		// If the transition is not instantaneous, set up the transitionCamera.
		if (transitionTime > 0) {
			transitionCamera = new FlxCamera(Registry.game.xOffset, Registry.game.yOffset, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT, Registry.game.ratio);
			transitionCamera.setBounds(oldRoom.x, oldRoom.y, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT);
			FlxG.cameras.add(transitionCamera);
		} else {
			
		// If the transition is instantaneous, just remove the old room.
			roomGroup.remove(oldRoom);
		}
		
		// Remove the old room's solids from the solids group.
		solids.remove(oldRoom.solids);
		
		// Unpause the new room.
		_room = newRoom;
		_room.paused = false;
		
		// Add the new room and its solids.
		roomGroup.add(_room);
		solids.add(_room.solids);
		
		// Set the camera and world bounds.
		FlxG.camera.setBounds(_room.x, _room.y, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT);
		FlxG.worldBounds.set(_room.x, _room.y, KagiNoMeiro.GAME_WIDTH, KagiNoMeiro.GAME_HEIGHT);
	}
	
	/**
	 * Play the animation where a lock steals the player's key.
	 */
	public function playKeyLossAnimation():Void {
		lockStealingKey.exists = true;
		lockStealingKey.x = _player.x;
		lockStealingKey.y = _player.y;
	}
	
	// Getters
	function get_room():Room {return _room;}
	public var room(get_room, null):Room;
	function get_player():Player {return _player;}
	public var player(get_player, null):Player;
}
