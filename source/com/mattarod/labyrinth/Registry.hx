package com.mattarod.labyrinth;

import com.mattarod.labyrinth.gameobjects.Book;
import com.mattarod.labyrinth.gameobjects.Door;
import com.mattarod.labyrinth.gameobjects.Key;
import com.mattarod.labyrinth.gameobjects.Lock;
import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.states.GameState;
import de.polygonal.ds.ListSet.ListSet;

/**
 * A class containing references to the game's "global variables."
 *
 * All members are static.
 *
 * Most of these are set up by the GameClass when it loads the game.
 */
class Registry {
	/**
	 * A reference to the current GameState.
	 */
	public static var gameState:GameState;
	
	/**
	 * The number of rooms wide the dungeon is.
	 */
	public static var roomsWide:Int;
	
	/**
	 * The number of rooms tall the dungeon is.
	 */
	public static var roomsTall:Int;
	
	/**
	 * A 2D Array containing all of the rooms in the dungeon.
	 * A row of columns of rooms.
	 */
	public static var rooms:Array<Array<Room>>;
	
	/**
	 * A reference to the starting room.
	 */
	public static var startRoom:Room;
	
	/**
	 * The x-position in the starting room where the player starts.
	 */
	public static var startPositionX:Float;
	
	/**
	 * The y-position in the starting room where the player starts.
	 */
	public static var startPositionY:Float;
	
	/**
	 * A disorganized ListSet of the brother doors in the game.
	 */
	public static var brotherDoorListSet:ListSet<Door>;
	
	/**
	 * A list of strings for starting brotherbooks.
	 */
	public static var brotherIntros:Array<String>;
	
	/**
	 * An array of brotherbooks, indexed by brother index.
	 */
	public static var brotherBookArray:Array<Book>;
	
	/**
	 * An array of generalbooks, indexed by trait index.
	 */
	public static var generalBookArray:Array<Book>;
	
	/**
	 * A reference to the key.
	 */
	public static var key:Key;
	
	/**
	 * A ListSet of rooms that need to be reset when the key is picked up.
	 */
	public static var resetRooms:ListSet<Room>;
	
	/**
	 * A reference to the lock that took the key,
	 * so it can be restored once the key is taken.
	 */
	public static var lockThatTookKey:Lock;
	
	/**
	 * An array of flags for level scripts to use.
	 */
	public static var flags:Array<Bool>;
	
	/**
	 * A reference to the game object.
	 */
	public static var game:KagiNoMeiro;
}
