package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import com.mattarod.labyrinth.gameObjects.scripts.Script;
import com.mattarod.labyrinth.gameObjects.scripts.ScriptRunner;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * An in-game object representing a button.
 * It executes a script when pressed and released.
 *
 * @author Matthew Rodriguez
 */
class Button extends FlxSprite implements FlxInteractiveSprite implements FlxActivatableSprite implements FlxResettableSprite {
	/**
	 * The width of a floor-mounted button's hitbox.
	 */
	public static inline var FLOOR_WIDTH:Float = KagiNoMeiro.TILE_LENGTH;
	
	/**
	 * The height of a floor-mounted button's hitbox.
	 */
	public static inline var FLOOR_HEIGHT:Float = 1;
	
	/**
	 * The x-offset of a floor-mounted button's hitbox.
	 */
	public static inline var FLOOR_X_OFFSET:Float = 0;
	
	/**
	 * The y-offset of a floor-mounted button's hitbox.
	 */
	public static inline var FLOOR_Y_OFFSET:Float = KagiNoMeiro.TILE_LENGTH - FLOOR_HEIGHT;
	
	private var onPress:Script;
	private var onRelease:Script;
	
	private var onPressScriptRunner:ScriptRunner;
	private var onReleaseScriptRunner:ScriptRunner;
	
	private var sticky:Bool;
	private var onFloor:Bool;
	
	private var beingPressed:Bool = false;
	private var pressed:Bool = false;
	
	private var originalX:Float;
	private var originalY:Float;
	private var originalName:String;
	private var originalWidth:Int;
	private var originalHeight:Int;
	
	/**
	 * Create a new Button.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	x The x-position of the button.
	 * @param	y The y-position of the button.
	 * @param	name Load "assets/png/[name]button.png"
	 * @param	onPress The name of the script to execute when pressed.
	 * @param	onRelease The name of the script to execute when released.
	 * @param	sticky Whether the script sticks in when pressed.
	 * @param	kind "floor" for floor-mounted, "area" for area button
	 * @param	width The width of the sprite's graphics.
	 * @param	height The height of the sprite's graphics.
	 */
	public function new(room:Room, x:Float = 0, y:Float = 0, name:String = "red", onPress:String = "null", onRelease:String = "null", sticky:String = "notsticky", kind = "floor", width:Int = 16, height:Int = 16) {
		// Initialize the FlxSprite.
		super();
		
		// Record the constructor parameters for later resetting.
		originalX = x;
		originalY = y;
		originalName = name;
		originalWidth = width;
		originalHeight = height;
		
		// Set the stickiness of the button
		this.sticky = switch(sticky) {
			case "sticky": true;
			case "nonsticky": false;
			default: throw "Invalid sticky parameter: " + sticky;
		}
		
		// Set the kind of button
		this.onFloor = switch(kind) {
			case "floor": true;
			case "area": false;
			default: throw "Invalid kind parameter: " + kind;
		}
		
		// Set up scripts
		this.onPress = new Script(onPress, room);
		this.onRelease = new Script(onRelease, room);
		
		// Initialize all resettable fields.
		resetScenario();
	}
	
	/**
	 * Reset this button to its original conditions.
	 */
	public function resetScenario() {
		// Load the graphics
		
		// Load a floor-mounted button's graphics
		if(onFloor) {
			loadGraphic(Assets.getBitmapData("assets/png/" + originalName + "button.png"),
				true, false, KagiNoMeiro.TILE_LENGTH, KagiNoMeiro.TILE_LENGTH);
				
		// Load an area button's graphics
		} else {
			loadGraphic(Assets.getBitmapData("assets/png/" + originalName + "button.png"),
				true, false, originalWidth, originalHeight);
		}
		
		animation.add("notpressed", [0], 1, false);
		animation.add("pressed", [1], 1, false);
		
		animation.play("notpressed");
		
		// Set dimensions
		if(onFloor) {
			width = FLOOR_WIDTH;
			height = FLOOR_HEIGHT;
			offset.x = FLOOR_X_OFFSET;
			offset.y = FLOOR_Y_OFFSET;
			
			x = originalX + FLOOR_X_OFFSET;
			y = originalY + FLOOR_Y_OFFSET;
		} else {
			offset.x = 0;
			offset.y = 0;
			
			x = originalX;
			y = originalY;
		}
		
		// Reset velocity
		velocity.x = 0;
		velocity.y = 0;
		
		// Reset script runners
		onPressScriptRunner = null;
		onReleaseScriptRunner = null;
		
		// Reset scripts
		onPress.reset();
		onRelease.reset();
		
		// Reset pressed
		beingPressed = false;
		pressed = false;
	}
	
	/**
	 * Called when the player collides with this button.
	 * Press this button if it isn't already pressed.
	 *
	 * @param	player The player.
	 */
	public function onPlayerCollision():Void {
		// Do nothing if this button was being pressed last frame
		if (beingPressed) return;

		beingPressed = true;
		
		// Add this to the set of buttons being pressed by the player.
		Registry.gameState.player.touchingButtons.set(this);
		
		// Press the button (unless this button is sticky and already pressed)
		if(!(sticky && pressed)) {
			press();
		}
	}
	
	/**
	 * Update function.
	 */
	public override function update():Void {
		// Update the parent FlxSprite.
		super.update();
		
		// Update the delegate ScriptRunners, if they exist.
		if (onPressScriptRunner != null) {
			onPressScriptRunner.update();
		}
		
		if (onReleaseScriptRunner != null) {
			onReleaseScriptRunner.update();
		}
	}
	
	/**
	 * Press the button.
	 */
	private function press():Void {
		pressed = true;
		
		animation.play("pressed");
		
		if(onFloor)	FlxG.sound.play("ButtonPress");
		
		// If the onPressScriptRunner doesn't exist, create it.
		if (onPressScriptRunner == null) {
			onPressScriptRunner = new ScriptRunner(onPress);
			
		// If it does exist but is finished, create a new one and reset the script.
		} else if (onPressScriptRunner != null && onPressScriptRunner.isFinished()) {
			onPress.reset();
			onPressScriptRunner = new ScriptRunner(onPress);
		}
	}
	
	/**
	 * Release the button.
	 */
	private function release():Void {
		pressed = false;
		
		animation.play("notpressed");
		
		if(onFloor)	FlxG.sound.play("ButtonRelease");
		
		// If the onReleaseScriptRunner doesn't exist, create it.
		if (onReleaseScriptRunner == null) {
			onReleaseScriptRunner = new ScriptRunner(onRelease);
			
		// If it does exist but is finished, create a new one and reset the script.
		} else if (onReleaseScriptRunner != null && onReleaseScriptRunner.isFinished()) {
			onRelease.reset();
			onReleaseScriptRunner = new ScriptRunner(onRelease);
		}
	}
	
	/**
	 * Called when the player stops colliding with the button.
	 * Release the button, unless it is sticky.
	 */
	public function stepOff():Void {
		beingPressed = false;
		
		if(!sticky) {
			release();
		}
	}
	
	/**
	 * Set whether the button is pressed.
	 * Used to stick or unstick a sticky button;
	 * Does nothing for non-sticky buttons.
	 * @param	b false if pressed, true otherwise
	 */
	public function setSwitchActive(b:Bool):Void {
		if (sticky && pressed && b) {
			beingPressed = false;
			pressed = false;
			animation.play("notpressed");
		} else if (sticky && !pressed && !b) {
			pressed = true;
			animation.play("pressed");
		}
	}
	
	/**
	 * Toggle whether the button is pressed.
	 * Used to stick or unstick a sticky button;
	 * Does nothing for non-sticky buttons.
	 */
	public function toggleSwitch():Void {
		setSwitchActive(pressed);
	}
}
