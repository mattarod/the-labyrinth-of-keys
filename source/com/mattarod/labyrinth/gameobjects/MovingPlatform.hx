package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import com.mattarod.labyrinth.KagiNoMeiro;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import openfl.Assets;

/**
 * An in-game object representing a moving platform.
 *
 * @author Matthew Rodriguez
 */
class MovingPlatform extends FlxSprite implements FlxActivatableSprite implements FlxResettableSprite {
	
	private var path:Array<FlxPoint>;
	private var iteration:Int;
	private var speed:Float;
	private var timer:Float;
	
	private var switchActive:Bool;
	private var moving = true;
	
	private var tilesWide:Int;
	private var tilesHigh:Int;
	
	/**
	 * Create a new moving platform.
	 * 
	 * @param	rawPath The path that the platform will follow as a flat array.
	 * @param	speed The speed of the platform in positions per second.
	 * @param	tilesWide The width of the platform in tiles.
	 * @param	tilesHigh The height of the platform in tiles.
	 * @param	room The room this platform is in.
	 */
	public function new(rawPath:Array<String>, speed:Float, tilesWide:Int, tilesHigh:Int, room:Room) {
		super();
		
		this.speed = speed;
		this.tilesHigh = tilesHigh;
		this.tilesWide = tilesWide;
		
		immovable = true;
		
		// Convert path from array of strings to array of FlxPoints.
		path = new Array<FlxPoint>();
		
		for (i in 0...Math.floor(rawPath.length/2)) {
			var stepX:Float = Std.parseFloat(rawPath[2 * i]);
			var stepY:Float = Std.parseFloat(rawPath[2 * i + 1]);
			
			path.push(new FlxPoint(
				stepX * KagiNoMeiro.TILE_LENGTH + room.x,
				stepY * KagiNoMeiro.TILE_LENGTH + room.y
			));
		}
		
		// Initialize all resettable fields.
		resetScenario();
	}
	
	/**
	 * Reset this platform to its original conditions.
	 */
	public function resetScenario():Void {
		// Load the graphics
		loadGraphic(Assets.getBitmapData("assets/png/plat" + tilesWide + "x" + tilesHigh + ".png"));
		
		// Set dimensions
		x = path[0].x;
		y = path[0].y;
		
		// Reset to beginning of path
		iteration = path.length - 1;
		timer = 0;
		
		switchActive = true;
		visible = true;
		moving = false;
		
		velocity.x = 0;
		velocity.y = 0;
	}
	
	/**
	 * Update function.
	 */
	public override function update():Void {
		super.update();
		
		// If this object is turned off,
		// and not currently moving between points in the path,
		// do nothing.
		if (!switchActive && !moving) {
			return;
		}
		
		// Determine which step of the path this object is on,
		// based on the timer and the speed.
		var iterationThisFrame:Int = Math.floor(timer * speed) % path.length;
		
		// What to do when the iteration increments.
		if (iterationThisFrame != iteration) {
			
			// Snap to the next position on the list,
			// to prevent drifting.
			var snapToIndex = iterationThisFrame;
			x = path[snapToIndex].x;
			y = path[snapToIndex].y;
			
			// If the switch is off, stop moving.
			if (moving && !switchActive) {
				moving = false;
				velocity.x = 0;
				velocity.y = 0;
				return;
				
			} 
			
			moving = true;
			iteration = iterationThisFrame;
			
			// Determine the next destination.
			var destinationIndex:Int = (iterationThisFrame+1) % path.length;
			var destination:FlxPoint = path[destinationIndex];
			
			// Set the velocity so that the platform reaches the destination
			// at the end of this iteration.
			velocity.x = (destination.x - x) * speed;
			velocity.y = (destination.y - y) * speed;
		}
		
		// Update the timer if the object is moving.
		if(moving) timer += FlxG.elapsed;
	}
	
	/**
	 * Set whether the platform is moving.
	 *
	 * @param	b
	 */
	public function setSwitchActive(b:Bool):Void {
		switchActive = b;
	}
	
	/**
	 * Toggle whether the platform is moving.
	 */
	public function toggleSwitch():Void {
		switchActive = !switchActive;
	}
}
