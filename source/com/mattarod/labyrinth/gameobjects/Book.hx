package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import openfl.Assets;

/**
 * An in-game object that displays text when the player walks in front of it.
 * Only legible when the player holds the key.
 *
 * @author Matthew Rodriguez
 */
class Book extends FlxSprite implements FlxInteractiveSprite {
	
	/**
	 * The width of the book sprite.
	 */
	public static inline var WIDTH:Float = KagiNoMeiro.TILE_LENGTH;
	
	/**
	 * The height of the book sprite.
	 */
	public static inline var HEIGHT:Float = KagiNoMeiro.TILE_LENGTH;
	
	private var _text:String = "";
	private var _open:Bool = false;
	private var _read:Bool = false;
	private var sound:FlxSound = null;
	
	/**
	 * Create a new Book.
	 *
	 * @param	x The x-position of the Book in pixels
	 * @param	y The y-position of the Book in pixels
	 * @param	textPath Load text "assets/text/[textPath].txt"
	 * @param	name Load sprite "assets/png/[name]book.png"
	 */
	public function new(x:Float = 0, y:Float = 0, textPath:String = null, name:String = "") {
		
		// Initialize the FlxSprite.
		super(x, y);
		
		// Load the graphics.
		loadGraphic(Assets.getBitmapData("assets/png/" + name + "book.png"),
			true, false, WIDTH, HEIGHT);
		
		animation.add("closed", [0], 1, false);
		animation.add("open", [1], 1, false);
		
		animation.play("closed");
		
		// Load the text.
		if (textPath != null) {
			appendFile(textPath);
		}
	}
	
	/**
	 * Append a string to this book's text.
	 * 
	 * @param	text The string to append.
	 */
	public function append(text:String) {
		_text += text;
	}
	
	/**
	 * Load string from file and append it to this book's text.
	 * 
	 * @param	textPath Load "assets/text/[textPath].txt"
	 */
	public function appendFile(textPath:String) {
		_text += Assets.getText("assets/text/" + textPath + ".txt");
	}
	
	/**
	 * Open the book if the player has the key.
	 */
	public function onPlayerCollision():Void {
		if (!_open && Registry.gameState.player.hasKey) {
			_read = true;
			_open = true;
			
			animation.play("open");
			
			if (sound != null) sound.stop();
			sound = FlxG.sound.play("OpenBook");
			
			Registry.gameState.player.touchingBook = this;
			Registry.gameState.openBook(_text);
		}
	}
	
	/**
	 * Close the book.
	 */
	public function close():Void {
		_open = false;
		
		animation.play("closed");
		
		if (sound != null) sound.stop();
		sound = FlxG.sound.play("CloseBook");
		
		Registry.gameState.player.touchingBook = null;
		Registry.gameState.closeBook();
	}
	
	// Getters
	function get_text():String { return _text; }
	public var text(get_text, null):String;
	function get_open():Bool {return _open;}
	public var open(get_open, null):Bool;
	function get_read():Bool {return _read;}
	function set_read(value:Bool):Bool {return _read = value;}
	public var read(get_read, set_read):Bool;
}
