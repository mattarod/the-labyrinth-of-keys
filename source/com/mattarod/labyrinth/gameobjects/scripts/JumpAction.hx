package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.Registry;

/**
 * A ScriptAction which causes the script to jump,
 * that is, set its execution point to a different point.
 * 
 * It can be an unconditional jump,
 * or a conditional jump that depends on a flag.
 *
 * @author Matthew Rodriguez
 */
class JumpAction implements ScriptAction {
	
	/**
	 * A variable representing the lack of a flag,
	 * used for an unconditional jump.
	 */
	public static inline var NO_FLAG:Int = -1;
	
	private var script:Script;
	private var jumpToIndex:Int;
	private var flagIndex:Int;
	
	/**
	 * Create the JumpAction.
	 * Leave the "flag" argument blank for unconditional jump.
	 *
	 * @param	script A reference to the script this JumpAction belongs to.
	 * @param	index Where to jump to.
	 * @param	flag Jump only if this flag is on.
	 */
	public function new(script:Script, jumpToIndex:Int, flagIndex:Int = NO_FLAG) {
		this.script = script;
		this.jumpToIndex = jumpToIndex;
		this.flagIndex = flagIndex;
	}
	
	/**
	 * If no flag is specified, jump.
	 * If flag is specified and on, jump.
	 */
	public function act():Void {
		if (flagIndex == NO_FLAG || Registry.flags[flagIndex]) {
			script.jump(jumpToIndex);
		}
	}
}
