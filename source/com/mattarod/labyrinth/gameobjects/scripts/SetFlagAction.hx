package com.mattarod.labyrinth.gameobjects.scripts;

import com.mattarod.labyrinth.gameObjects.scripts.ScriptAction;
import com.mattarod.labyrinth.Registry;

/**
 * A ScriptAction which turns on or off a flag.
 *
 * @author Matthew Rodriguez
 */
class SetFlagAction implements ScriptAction {

	private var index:Int;
	private var newValue:Bool;
	
	/**
	 * Create the ActivateAction.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	index The index of the object in the room's scenario data.
	 */
	public function new(index:Int, newValue:Bool) {
		this.index = index;
		this.newValue = newValue;
	}
	
	/**
	 * Activate the object.
	 */
	public function act():Void {
		Registry.flags[index] = newValue;
	}
}
