package com.mattarod.labyrinth.gameObjects.scripts;

/**
 * An interface representing an action that an in-game object
 * (usually a FlxSprite) performs as part of a Script.
 */
interface ScriptAction {
	
	/**
	 * Override this function to define what your ScriptAction does.
	 */
	public function act():Void;
}
