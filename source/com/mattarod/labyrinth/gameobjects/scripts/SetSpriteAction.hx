package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import flixel.FlxSprite;

/**
 * A ScriptAction that sets the sprite of a FlxSprite in the room.
 *
 * @author Matthew Rodriguez
 */
class SetSpriteAction implements ScriptAction {
	
	private var room:Room;
	private var index:Int;
	private var s:String;
	
	/**
	 * Creates the SetSpriteAction.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	index The index of the object in the room's scenario data.
	 * @param	s Set the sprite's image to "assets/png/[s].png"
	 */
	public function new(room:Room, index:Int, s:String) {
		this.room = room;
		this.index = index;
		this.s = s;
	}
	
	public function act():Void {
		var sprite:FlxSprite = room.getFlxSprite(index);
		sprite.loadGraphic("assets/png/" + s + ".png");
	}
}
