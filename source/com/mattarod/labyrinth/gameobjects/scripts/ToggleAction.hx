package com.mattarod.labyrinth.gameobjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.gameObjects.scripts.ScriptAction;

/**
 * A ScriptAction which toggles a FlxActivatableSprite in-game object.
 *
 * @author Matthew Rodriguez
 */
class ToggleAction implements ScriptAction {
	private var room:Room;
	private var index:Int;
	
	/**
	 * Create the ActivateAction.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	index The index of the object in the room's scenario data.
	 */
	public function new(room:Room, index:Int) {
		this.room = room;
		this.index = index;
	}
	
	/**
	 * Toggle the object.
	 */
	public function act():Void {
		room.getActivatable(index).toggleSwitch();
	}
}
