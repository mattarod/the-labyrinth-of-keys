package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.KagiNoMeiro;
import flixel.FlxSprite;

/**
 * A ScriptAction that sets the x,y position of a FlxSprite in the room.
 *
 * @author Matthew Rodriguez
 */
class SetPositionAction implements ScriptAction {
	
	private var room:Room;
	private var index:Int;
	private var x:Float;
	private var y:Float;
	
	/**
	 * Creates the SetPositionAction.
	 *
	 * @param	room The room which contains the object.
	 * @param	index The index of the object in the room.
	 * @param	x The x-position to set the object to (in tiles).
	 * @param	y The y-position to set the object to (in tiles).
	 */
	public function new(room:Room, index:Int, x:Float, y:Float) {
		this.x = (KagiNoMeiro.TILE_LENGTH * x) + room.x;
		this.y = (KagiNoMeiro.TILE_LENGTH * y) + room.y;
		this.room = room;
		this.index = index;
	}
	
	/**
	 * Set the object to the position.
	 */
	public function act():Void {
		var sprite:FlxSprite = room.getFlxSprite(index);
		
		sprite.x = x + sprite.offset.x;
		sprite.y = y + sprite.offset.y;
	}
}
