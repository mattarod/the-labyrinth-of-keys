package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.gameObjects.scripts.ScriptAction;
import flixel.FlxG;

/**
 * A ScriptAction which plays a sound effect.
 *
 * @author Matthew Rodriguez
 */
class PlaySoundAction implements ScriptAction {

	private var sound:String;
	
	/**
	 * Create a new PlaySoundAction.
	 *
	 * @param	sound The name of the sound to play (specified in Project.xml)
	 */
	public function new(sound:String) {
		this.sound = sound;
	}
	
	/**
	 * Play the sound.
	 */
	public function act():Void {
		FlxG.sound.play(sound);
	}
	
}
