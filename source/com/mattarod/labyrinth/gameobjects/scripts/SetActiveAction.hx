package com.mattarod.labyrinth.gameobjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.gameObjects.scripts.ScriptAction;

/**
 * A ScriptAction which turns on or off a FlxActivatableSprite in-game object.
 *
 * @author Matthew Rodriguez
 */
class SetActiveAction implements ScriptAction {

	private var room:Room;
	private var index:Int;
	private var newValue:Bool;
	
	/**
	 * Create the ActivateAction.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	index The index of the object in the room's scenario data.
	 * @param	newValue The new value of target's "active" property
	 */
	public function new(room:Room, index:Int, newValue:Bool) {
		this.room = room;
		this.index = index;
		this.newValue = newValue;
	}
	
	/**
	 * Activate the object.
	 */
	public function act():Void {
		room.getActivatable(index).setSwitchActive(newValue);
	}
}
