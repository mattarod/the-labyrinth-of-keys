package com.mattarod.labyrinth.gameObjects.scripts;

/**
 * An important control flow ScriptAction.
 *
 * This class can either represent a wait for a specific duration,
 * or a termination of the script.
 */
class WaitAction implements ScriptAction {

	/**
	 * The duration of the wait in seconds.
	 */
	public var duration:Float;
	
	/**
	 * Create the WaitAction.
	 * Default is FOREVER.
	 *
	 * @param	duration The amount of time to wait.
	 */
	public function new(duration:Float = Script.FOREVER) {
		this.duration = duration;
	}

	/**
	 * Do nothing.
	 */
	public function act():Void {
		return;
	}
}
