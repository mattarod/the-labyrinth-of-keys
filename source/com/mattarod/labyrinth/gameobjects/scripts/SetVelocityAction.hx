package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.KagiNoMeiro;
import flixel.FlxSprite;

/**
 * A ScriptAction that sets the x,y velocity of a FlxSprite in the room.
 *
 * @author Matthew Rodriguez
 */
class SetVelocityAction implements ScriptAction {
	
	private var room:Room;
	private var index:Int;
	private var x:Float;
	private var y:Float;
	
	/**
	 * Creates the SetVelocityAction.
	 *
	 * @param	room A reference to the room which contains the object.
	 * @param	index The index of the object in the room's scenario data.
	 * @param	x The x-velocity to set the object to (in tiles per second.)
	 * @param	y The y-velocity to set the object to (in tiles per second.)
	 */
	public function new(room:Room, index:Int, x:Float, y:Float) {
		this.room = room;
		this.index = index;
		this.x = x * KagiNoMeiro.TILE_LENGTH;
		this.y = y * KagiNoMeiro.TILE_LENGTH;
	}
	
	/**
	 * Set the FlxSprite's velocity.
	 */
	public function act():Void {
		var sprite:FlxSprite = room.getFlxSprite(index);
		
		// Snap to nearest pixel position to reduce potential drift
		sprite.x = Math.round(sprite.x);
		sprite.y = Math.round(sprite.y);
		
		sprite.velocity.x = x;
		sprite.velocity.y = y;
	}
}
