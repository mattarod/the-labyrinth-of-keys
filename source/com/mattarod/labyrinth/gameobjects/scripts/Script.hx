package com.mattarod.labyrinth.gameObjects.scripts;

import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.gameobjects.scripts.SetActiveAction;
import com.mattarod.labyrinth.gameobjects.scripts.SetFlagAction;
import com.mattarod.labyrinth.gameObjects.scripts.SetVelocityAction;
import com.mattarod.labyrinth.gameobjects.scripts.ToggleAction;
import openfl.Assets;

/**
 * A class which represents a script that an in-game object follows.
 *
 * Scriptable actions include:
	 * Activating and deactivating objects in the room
	 * Moving an object in the room
	 * Setting the velocity of an object in the room
	 * Changing the sprite of an object in the room
	 * Playing a sound
 *
 * There are also "flow control" actions:
	 * Jumping the execution point
	 * Jumping the execution point if a flag is on
	 * Waiting for a specified period of time
	 * Waiting forever (terminating the script)
 */
class Script {
	/**
	 * A constant representing the duration "FOREVER."
	 */
	public static inline var FOREVER:Float = -1;
	
	private var actions:Array<ScriptAction>;
	private var executionPoint:Int = 0;
	
	/**
	 * Create a new script.
	 * You can leave the arguments blank to create a blank script.
	 * 
	 * @param	scriptName The name of the script to load.
	 * @param	room A reference to the room that this script is running in.
	 */
	public function new(scriptName:String = null, room:Room = null) {
		actions = new Array<ScriptAction>();
		
		if (scriptName != null && room != null) {
			parse(scriptName, room);
		}
	}
	
	/**
	 * Parse script: assets/script/[scriptName].csv
	 *
	 * @param	scriptName The name of the script to load.
	 * @param	room A reference to the room that this script is running in.
	 */
	public function parse(scriptName:String, room:Room):Void {
		// Load the script from file.
		var script:String = Assets.getText("assets/script/" + scriptName + ".csv");
		
		// Split the script into lines.
		var scriptActions:Array<String> = script.split("\r\n");
		
		// Iterate over all of the lines in the file.
		for (scriptAction in scriptActions) {
			
			// Skip blank lines
			if (scriptAction == "") continue;
			
			// Parse the line as a comma-separated array.
			var params:Array<String> = scriptAction.split(",");
			
			switch(params[0]) {
				
				// activate,n
				// Activates the object at index n in the room.
				case "activate":
					if (params.length != 2) {
						throw("Invalid activate parameters: " + scriptAction);
					}
					
					addAction(new SetActiveAction(room, Std.parseInt(params[1]), true));
					
				// deactivate,n
				// Dectivates the object at index n in the room.
				case "deactivate":
					if (params.length != 2) {
						throw("Invalid deactivate parameters: " + scriptAction);
					}
					
					addAction(new SetActiveAction(room, Std.parseInt(params[1]), false));
					
				// toggle,n
				// Toggles the "active" property of the object at index n in the room.
				case "toggle":
					if (params.length != 2) {
						throw("Invalid toggle parameters: " + scriptAction);
					}
					
					addAction(new ToggleAction(room, Std.parseInt(params[1])));
					
				// wait,x
				// Waits x seconds before proceeding to the next ScriptAction.
				// wait
				// Wait forever. (Terminate the script.)
				case "wait":
					if (params.length == 1) {
						addAction(new WaitAction());
						
					} else if (params.length == 2) {
						addAction(new WaitAction(Std.parseFloat(params[1])));
						
					} else {
						throw("Invalid wait parameters: " + scriptAction);
					}
					
				// jump,n
				// Jump to line n.
				// jump,n,f
				// Jump to line n if flag f is on, else do nothing.
				case "jump":
					if (params.length == 2) {
						addAction(new JumpAction(this, Std.parseInt(params[1])));
					} else if(params.length == 3) {
						addAction(new JumpAction(this, Std.parseInt(params[1]), Std.parseInt(params[2])));
					} else {
						throw("Invalid jump parameters: " + scriptAction);
					}
					
				// playsound,s
				// Play the sound effect named "s".
				case "playsound":
					if (params.length != 2) {
						throw("Invalid playsound parameters: " + scriptAction);
					}
					
					addAction(new PlaySoundAction(params[1]));
					
				// setposition,n,x,y
				// Move the object at index n in the room to position x,y (in tiles)
				case "setposition":
					if (params.length != 4) {
						throw("Invalid setposition parameters: " + scriptAction);
					}
					
					addAction(new SetPositionAction(
						room,
						Std.parseInt(params[1]),
						Std.parseFloat(params[2]),
						Std.parseFloat(params[3])
					));
					
				// setvelocity,n,x,y
				// Set the velocity of the object at index n in the room to x,y (in tiles per second)
				case "setvelocity":
					if (params.length != 4) {
						throw("Invalid setvelocity parameters: " + scriptAction);
					}
					
					addAction(new SetVelocityAction(
						room,
						Std.parseInt(params[1]),
						Std.parseFloat(params[2]),
						Std.parseFloat(params[3])
					));
					
				// setsprite,n,s
				// Set the sprite of the object at index n in the room to assets/png/[s].png
				case "setsprite":
					if (params.length != 3) {
						throw("Invalid setsprite parameters: " + scriptAction);
					}
					
					addAction(new SetSpriteAction(
						room,
						Std.parseInt(params[1]),
						params[2]
					));
					
				// flagon,n
				// Turn on flag n
				case "flagon":
					if (params.length != 2) {
						throw("Invalid flagon parameters: " + scriptAction);
					}
					
					addAction(new SetFlagAction(
						Std.parseInt(params[1]),
						true
					));
					
				// flagoff,n
				// Turn off flag n
				case "flagoff":
					if (params.length != 2) {
						throw("Invalid flagoff parameters: " + scriptAction);
					}
					
					addAction(new SetFlagAction(
						Std.parseInt(params[1]),
						false
					));
					
				// The first word is not a recognized ScriptAction
				default:
					throw("Invalid script action " + scriptAction);
			}
		}
	}
	
	/**
	 * Add a ScriptAction to the Script.
	 *
	 * @param	action The action to add.
	 */
	public function addAction(action:ScriptAction):Void {
		actions.push(action);
	}
	
	/**
	 * Execute the current ScriptAction in the Script,
	 * increment the execution point, and return the executed action.
	 *
	 * @return The executed ScriptAction.
	 */
	public function step():ScriptAction {
		
		// If the execution point is at or past the end of the script,
		// return null.
		if (executionPoint >= actions.length) {
			return null;
		}
		
		// Get the current action in the script, and then
		// increment the execution point in the script forward one action.
		var currentAction:ScriptAction = actions[executionPoint++];
		
		// Execute the current action.
		currentAction.act();
		
		// Return the executed action.
		return currentAction;
	}
	
	/**
	 * Move the execution point to a given position in the script.
	 *
	 * @param	position The position to move the execution point to.
	 */
	public function jump(position:Int):Void {
		executionPoint = position;
	}
	
	/**
	 * Reset the script by moving the execution pointer back to the start.
	 */
	public function reset():Void {
		jump(0);
	}
	
	/**
	 * Determine if the script contains no actions.
	 *
	 * @return true if the script has no actions.
	 */
	public function isEmpty():Bool {
		return actions.length == 0;
	}
}
