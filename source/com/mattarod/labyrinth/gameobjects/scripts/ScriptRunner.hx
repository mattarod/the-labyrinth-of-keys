package com.mattarod.labyrinth.gameObjects.scripts;

import flixel.FlxG;

/**
 * A class which runs a script on behalf of an in-game object.
 *
 * @author Matthew Rodriguez
 */
class ScriptRunner {

	/**
	 * A constant representing the point in time "NEVER".
	 */
	public static inline var NEVER:Float = -1;
	
	private var script:Script;
	private var nextActionTime:Float = 0.0;
	private var timer:Float = 0.0;
	
	/**
	 * Create a new ScriptRunner for a script.
	 * @param	script
	 */
	public function new(script:Script) {
		this.script = script;
		
		execute();
	}
	
	/**
	 * Execute the script.
	 */
	private function execute():Void {
		// Get the next action.
		var currentAction:ScriptAction;
		
		// Execute all actions in the script until the execution point 
		// reaches a wait action or the end of the script.
		// (All actions except WaitActions execute instantly.)
		do {
			currentAction = script.step();
		} while (currentAction != null && !Std.is(currentAction, WaitAction));
		
		// If the execution point reached the end of the script, terminate.
		if (currentAction == null) {
			nextActionTime = NEVER;
			return;
		}
		
		// Load the duration from the current WaitAction.
		var waitAction:WaitAction = cast(currentAction);
		
		if (waitAction.duration == Script.FOREVER) {
			// Terminate the script.
			nextActionTime = NEVER;
		} else {
			// Wait for the specified amount of time.
			nextActionTime = timer + waitAction.duration;
		}
	}
	
	/**
	 * Call this function every frame from the parent object.
	 *
	 * This is necessary for executing the script.
	 */
	public function update():Void {
		// If it's time to execute the next action, do so.
		if (nextActionTime != NEVER && timer + (FlxG.elapsed / 2) >= nextActionTime) {
				execute();
		}
		
		// Increment the timer.
		timer += FlxG.elapsed;
	}
	
	/**
	 * Determine if the script has terminated.
	 *
	 * @return true if finished, else false
	 */
	public function isFinished():Bool {
		return nextActionTime == NEVER;
	}
}
