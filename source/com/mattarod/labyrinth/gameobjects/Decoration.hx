package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * A sprite that exists purely as decoration.
 * Does not move, is not solid.
 * 
 * @author Matthew Rodriguez
 */
class Decoration extends FlxSprite implements FlxActivatableSprite implements FlxResettableSprite {
	
	private var originalX:Float;
	private var originalY:Float;
	private var originalName:String;
	
	/**
	 * Create a new Decoration.
	 *
	 * @param	x The x-position in pixels
	 * @param	y The y-position in pixels
	 * @param	name Load "assets/png/[name].png"
	 */
	public function new(x:Float = 0, y:Float = 0, name:String = "torch") {
		super();
		
		// Record the constructor parameters for later resetting.
		originalX = x;
		originalY = y;
		originalName = name;
		
		// Set immovable.
		immovable = true;
		
		// Initialize all resettable fields
		resetScenario();
	}
	
	public function resetScenario():Void {
		// Reset position
		x = originalX;
		y = originalY;
		
		// Reset velocity
		velocity.x = 0;
		velocity.y = 0;
		
		// Load sprite
		pixels = Assets.getBitmapData("assets/png/" + originalName + ".png");
		
		// Set visible
		visible = true;
	}
	
	/**
	 * Set this object's "visible" property.
	 *
	 * @param	b The new value for "visible."
	 */
	public function setSwitchActive(b:Bool):Void {
		visible = b;
	}
	
	/**
	 * Toggle this object's "visible" property.
	 */
	public function toggleSwitch():Void {
		visible = !visible;
	}
}
