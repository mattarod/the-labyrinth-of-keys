package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.Config;
import com.mattarod.labyrinth.gameobjects.Button;
import com.mattarod.labyrinth.Registry;
import de.polygonal.ds.ListSet.ListSet;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;

/**
 * An extended FlxSprite that represents the player character.
 */
class Player extends FlxSprite {
	/**
	 * The width of the player hitbox.
	 */
	public static inline var WIDTH:Float = 5;
	
	/**
	 * The height of the player hitbox.
	 */
	public static inline var HEIGHT:Float = 14;
	
	/**
	 * The width of the player sprite.
	 */
	public static inline var SPRITE_WIDTH:Float = 21;
	
	/**
	 * The height of the player sprite.
	 */
	public static inline var SPRITE_HEIGHT:Float = 16;
	
	/**
	 * The x-offset of the sprite.
	 */
	public static inline var X_OFFSET:Float = 8;
	
	/**
	 * The y-offset of the sprite.
	 */
	public static inline var Y_OFFSET:Float = 2;
	
	// Movement constants
	public static inline var WALK_SPEED:Int = 96; // Pixels per second
	public static inline var WALK_ACCELERATION:Int = 768; // Pixels per second squared
	public static inline var FALL_ACCELERATION:Int = 592; // Pixels per second squared
	public static inline var JUMP_SPEED = 240; // Pixels per second
	public static inline var TERMINAL_VELOCITY = 360; // Pixels per second
	
	private var _sideCollisionBox:FlxObject;
	
	private var _hasKey:Bool = false;
	private var _touchingBook:Book = null;
	private var _touchingButtons:ListSet<Button>;
	private var lastAnimation:String;
	private var _crushed:Bool = false;
	
	public function new() {
		super();
		
		_touchingButtons = new ListSet<Button>();
		
		// Load the spritesheet and animations
		loadGraphic("assets/png/mc.png", false, true, SPRITE_WIDTH, SPRITE_HEIGHT);
		
		// Animations without key
		animation.add("n-stand", [0], 1, false);
		animation.add("n-walk", [0, 1, 0, 2], 10, true);
		animation.add("n-jump", [3], 1, false);
		animation.add("crush", [8], 1, false);
		
		// Animations with key
		animation.add("k-stand", [4], 1, false);
		animation.add("k-walk", [4, 5, 4, 6], 10, true);
		animation.add("k-jump", [7], 1, false);
		
		animation.play("n-stand");
		lastAnimation = "n-stand";
		
		// Create the wall collision object.
		_sideCollisionBox = new FlxObject();
		_sideCollisionBox.width = WIDTH;
		_sideCollisionBox.height = HEIGHT;
		_sideCollisionBox.allowCollisions = FlxObject.WALL;
		
		// Set dimensions
		_offset.x = X_OFFSET;
		_offset.y = Y_OFFSET;
		
		width = WIDTH;
		height = HEIGHT;
		
		setPosition(Registry.startPositionX, Registry.startPositionY - HEIGHT);

		velocity.x = 0;
		velocity.y = 0;
		
		// Set physics constants
		maxVelocity.x = WALK_SPEED;
		maxVelocity.y = TERMINAL_VELOCITY;
		acceleration.y = FALL_ACCELERATION;
		drag.x = WALK_ACCELERATION;
		
		// Initialize player as touching the floor
		// in order to not play the landing sound effect at start of game
		wasTouching = FlxObject.FLOOR;
		touching = FlxObject.FLOOR;
	}
	
	/**
	 * Update method.
	 */
	public override function update():Void {
		
		// Do nothing except update the parent FlxSprite if crushed.
		if (crushed) {
			super.update();
			return;
		}
		
		// Move left if player is pressing left, not pressing right,
		// and isn't touching a wall on the left.
		if (FlxG.keyboard.pressed(Config.moveLeft) && !FlxG.keyboard.pressed(Config.moveRight) && !isTouching(FlxObject.LEFT)) {
			acceleration.x = -WALK_ACCELERATION;
		}
		
		// Move right if player is pressing right, not pressing left,
		// and isn't touching a wall on the right.
		else if (FlxG.keyboard.pressed(Config.moveRight) && !FlxG.keyboard.pressed(Config.moveLeft) && !isTouching(FlxObject.RIGHT)) {
			acceleration.x = WALK_ACCELERATION;
		}
		
		// Otherwise, stand still.
		else {
			acceleration.x = 0;
		}
		
		// Jump if touching floor and the player just pressed the jump button.
		if (FlxG.keyboard.justPressed(Config.jump) && isTouching(FlxObject.FLOOR)) {
			velocity.y = -JUMP_SPEED;
			FlxG.sound.play("Jump");
		}
		
		// Release jump if the player released the jump button
		// and still has upward velocity.
		if (FlxG.keyboard.justReleased(Config.jump) && velocity.y < 0) {
			velocity.y = 0;
		}
		
		// Play landing sound effect upon landing.
		if (justTouched(FlxObject.FLOOR)) {
			FlxG.sound.play("Land");
		}
		
		// Determine animation to play this frame
		var desiredAnimation:String = "";
		
		if (isTouching(FlxObject.FLOOR)) {
			if (velocity.x == 0) {
				// Touching floor and no velocity = standing
				desiredAnimation = "stand";
			} else {
				
				// Touching floor and horizontal velocity = walking
				desiredAnimation = "walk";
			}
		} else {
			
			// In air = jumping
			desiredAnimation = "jump";
		}
		
		// Determine which direction to face.
		if (velocity.x > 0) {
			facing = FlxObject.RIGHT;
		} else if (velocity.x < 0) {
			facing = FlxObject.LEFT;
		}
		
		// If the desired animation is not the current animation,
		// play the desired animation.
		if (desiredAnimation != lastAnimation) {
			if (_hasKey) desiredAnimation = "k-" + desiredAnimation;
			else desiredAnimation = "n-" + desiredAnimation;
			
			animation.play(desiredAnimation);
			lastAnimation = desiredAnimation;
		}
		
		// Update FlxSprite
		super.update();
		
		// Update _sideCollisionBox's position, velocity, and acceleration
		// to mirror this object.
		_sideCollisionBox.x = x;
		_sideCollisionBox.y = y;
		_sideCollisionBox.velocity.x = velocity.x;
		_sideCollisionBox.velocity.y = velocity.y;
		_sideCollisionBox.acceleration.x = acceleration.x;
		_sideCollisionBox.acceleration.y = acceleration.y;
		
		// Close a book when the player stops touching it.
		if (touchingBook != null && !FlxG.overlap(this, touchingBook)) {
			touchingBook.close();
		}
		
		// Release a button when the player stops touching it.
		for (button in touchingButtons) {
			if (!FlxG.overlap(this, button)) {
				button.stepOff();
				touchingButtons.remove(button);
			}
		}
	}
	
	/**
	 * Reset some state on warp.
	 */
	public function resetOnWarp():Void {
		FlxG.sound.destroySounds();
		FlxG.sound.play("Warp");
		
		setPosition(Registry.startPositionX, Registry.startPositionY - HEIGHT);
		
		velocity.x = 0;
		velocity.y = 0;
		
		_sideCollisionBox.velocity.x = 0;
		_sideCollisionBox.velocity.y = 0;
		
		_sideCollisionBox.wasTouching = FlxObject.FLOOR;
		_sideCollisionBox.touching = FlxObject.FLOOR;
		
		wasTouching = FlxObject.FLOOR;
		touching = FlxObject.FLOOR;
		
		_crushed = false;
		
		if (_hasKey) {
			Registry.key.exists = true;
			_hasKey = false;
		}
	}
	
	/**
	 * Set the position of this sprite as well as the side collision box.
	 *
	 * @param	x The new X coordinate.
	 * @param	y The new Y coordinate.
	 */
	public override function setPosition(x:Float = 0, y:Float = 0):Void {
		super.setPosition(x, y);
		_sideCollisionBox.setPosition(x, y);
	}
	
	/**
	 * Play the crushed animation and go into the crushed state.
	 */
	public function crush() {
		animation.play("crush");
		FlxG.sound.play("Crush");
		_crushed = true;
		velocity.y = -400;
	}
	
	// Getters and setters
	function get_hasKey():Bool {return _hasKey;}
	function set_hasKey(value:Bool):Bool {return _hasKey = value;}
	public var hasKey(get_hasKey, set_hasKey):Bool;
	function get_touchingBook():Book {return _touchingBook;}
	function set_touchingBook(value:Book):Book {return _touchingBook = value;}
	public var touchingBook(get_touchingBook, set_touchingBook):Book;
	function get_touchingButtons():ListSet<Button> {return _touchingButtons;}
	public var touchingButtons(get_touchingButtons, null):ListSet<Button>;
	function get_sideCollisionBox():FlxObject {return _sideCollisionBox;}
	public var sideCollisionBox(get_sideCollisionBox, null):FlxObject;
	function get_crushed():Bool {return _crushed;}
	public var crushed(get_crushed, null):Bool;
}
