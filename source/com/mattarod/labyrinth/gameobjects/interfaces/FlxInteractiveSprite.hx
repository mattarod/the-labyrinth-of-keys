package com.mattarod.labyrinth.gameobjects.interfaces;

import com.mattarod.labyrinth.gameobjects.Player;

/**
 * An interface which represents an in-game object (usually a FlxSprite)
 * which does something when it collides with the player.
 *
 * Switches, enemies, and so on all implement this interface.
 *
 * @author Matthew Rodriguez
 */
interface FlxInteractiveSprite {
	
	/**
	 * Override this function to determine what your object does
	 * when it collides with the player.
	 */
	public function onPlayerCollision():Void;
	
}
