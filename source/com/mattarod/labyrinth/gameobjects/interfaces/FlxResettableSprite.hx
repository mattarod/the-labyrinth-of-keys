package com.mattarod.labyrinth.gameobjects.interfaces;

/**
 * An interface which represents an in-game object (usually a FlxSprite) which
 * has state which should be reset when the room it is in is reset.
 *
 * This can be an enemy that needs to respawn, a platform which needs to
 * return to its original position, a button that needs to be unpressed,
 * and so on.
 *
 * @author Matthew Rodriguez
 */
interface FlxResettableSprite {
	
	/**
	 * Override this function with your scenario-resetting code.
	 */
	public function resetScenario():Void;
}
