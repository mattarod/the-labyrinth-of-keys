package com.mattarod.labyrinth.gameobjects.interfaces;

/**
 * An interface which represents an in-game object (usually a FlxSprite) that
 * can be turned "on" and "off" by a script.
 *
 * "Activated" can mean different things for different objects. This is a
 * generic interface that can be used for many kinds of game objects.
 *
 * For an enemy, "active" might mean "awake." 
 * For a light, "active" might mean "on."
 *
 * @author Matthew Rodriguez
 */
interface FlxActivatableSprite {
	
	/**
	 * If b is true, turn the object on.
	 * If b is false, turn the object off.
	 *
	 * @param	b What value to set the object's "active" parameter to.
	 */
	public function setSwitchActive(b:Bool):Void;
	
	/**
	 * Toggle the object's "active" property.
	 *
	 * If the object is off, turn it on.
	 * If the object is on, turn it off.
	 */
	public function toggleSwitch():Void;
}
