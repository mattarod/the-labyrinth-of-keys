package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * A class representing a solid, immovable, rectangular block of any size,
 * whose "exists" property can be toggled by scripts.
 *
 * @author Matthew Rodriguez
 */
class Block extends FlxSprite implements FlxActivatableSprite implements FlxResettableSprite {
	
	private var originalX:Float;
	private var originalY:Float;
	private var originalName:String;
	
	/**
	 * Create a new block.
	 *
	 * @param	x The x-position of the block in pixels
	 * @param	y The y-position of the block in pixels
	 * @param	name Load "assets/png/[name]block.png"
	 */
	public function new(x:Float = 0, y:Float = 0, name:String = "red") {
		super();
		
		// Record the initial state of the block.
		originalX = x;
		originalY = y;
		originalName = name;
		
		// Set immovable.
		immovable = true;
		
		// Initialize all resettable fields.
		resetScenario();
	}
	
	/**
	 * Reset this block to its original conditions.
	 */
	public function resetScenario():Void {
		// Reset position
		x = originalX;
		y = originalY;
		
		// Reset velocity
		velocity.x = 0;
		velocity.y = 0;
		
		// Load sprite
		pixels = Assets.getBitmapData("assets/png/" + originalName + "block.png");
		
		// Set exists
		exists = true;
	}
	
	/**
	 * Set this object's "exists" property.
	 *
	 * @param	b The new value for "exists."
	 */
	public function setSwitchActive(b:Bool):Void {
		exists = b;
	}
	
	/**
	 * Toggle this object's "exists" property.
	 */
	public function toggleSwitch():Void {
		exists = !exists;
	}
}
