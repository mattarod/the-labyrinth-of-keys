package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * An in-game object representing the key.
 *
 * @author Matthew Rodriguez
 */
class Key extends FlxSprite implements FlxInteractiveSprite {

	/**
	 * Create the Key.
	 *
	 * @param	x The x-position of the key
	 * @param	y The y-position of the key
	 */
	public function new(x:Float = 0, y:Float = 0) {
		super(x, y);
		
		loadGraphic(Assets.getBitmapData("assets/png/key.png"));
		
		// Create a reference to this object in the Registry.
		Registry.key = this;
	}
	
	/**
	 * Called when the player collides with this object.
	 * Pick up the key, and perform a global reset.
	 */
	public function onPlayerCollision():Void {
		this.exists = false;
		
		Registry.gameState.player.hasKey = true;
		
		FlxG.sound.play("Get");
		
		// Perform a global reset.
		for (room in Registry.resetRooms) {
			room.resetScenario();
		}
		
		// Restore the Lock that took the Key,
		// if that happened.
		if(Registry.lockThatTookKey != null) {
			Registry.lockThatTookKey.visible = true;
		}
	}
}
