package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.Config;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import com.mattarod.labyrinth.states.VictoryScreen;
import flixel.FlxG;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * An in-game object representing a door.
 * Destroys the key permanently when used.
 * May trigger the game's victory state.
 *
 * @author Matthew Rodriguez
 */
class Door extends FlxSprite implements FlxInteractiveSprite {
	
	/**
	 * The width of the book sprite.
	 */
	public static inline var WIDTH:Float = KagiNoMeiro.TILE_LENGTH;
	
	/**
	 * The height of the book sprite.
	 */
	public static inline var HEIGHT:Float = KagiNoMeiro.TILE_LENGTH;
	
	/**
	 * A value for brotherDoorIndex that represents a dummy door,
	 * one which can never be the exit.
	 */
	public static inline var DUMMY_DOOR = -1;
	
	/**
	 * A value for doorGroup that represents belonging to no group.
	 */
	public static inline var NONE = -1;
	
	private var _brotherDoorIndex:Int;
	private var _isExit:Bool = false;
	private var _doorGroup:Int = NONE;
	
	public function new(x:Float = 0, y:Float = 0, brotherDoorIndex:Int = DUMMY_DOOR) {
		// Initialize the FlxSprite
		super(x, y);
		
		// Load the graphics.
		loadGraphic(Assets.getBitmapData("assets/png/door.png"),
			true, false, WIDTH, HEIGHT);
		
		animation.add("locked", [0], 1, false);
		animation.add("open", [1], 1, false);
		
		animation.play("locked");
		
		// Set the brotherDoorIndex.
		_brotherDoorIndex = brotherDoorIndex;
	}
	
	/**
	 * Called when the player collides with this door.
	 * Use the key if the UseKey button is pressed.
	 */
	public function onPlayerCollision():Void {
		if (FlxG.keyboard.justPressed(Config.useKey) && Registry.gameState.player.hasKey) {
			// Destroy the key.
			Registry.gameState.player.hasKey = false;
			
			// If this door is the exit,
			// and the right brotherbook and all generalbooks are read, win.
			if (_isExit) {
				var allBooksRead:Bool = true;
				
				for (book in Registry.brotherBookArray) {
					allBooksRead = allBooksRead && book.read;
				}
				
				for (book in Registry.generalBookArray) {
					allBooksRead = allBooksRead && book.read;
				}
				
				if(allBooksRead) {
					FlxG.switchState(new VictoryScreen());
					return;
				}
			}
			
			// Open the door.
			FlxG.sound.play("Unlock");
			animation.play("open");
			FlxG.sound.music.stop();
		}
	}
	
	// Getters and setters
	function get_brotherDoorIndex():Int {return _brotherDoorIndex;}
	public var brotherDoorIndex(get_brotherDoorIndex, null):Int;
	function get_isExit():Bool {return _isExit;}
	function set_isExit(value:Bool):Bool {return _isExit = value;}
	public var isExit(get_isExit, set_isExit):Bool;
	function get_doorGroup():Int {return _doorGroup;}
	function set_doorGroup(value:Int):Int {return _doorGroup = value;}
	public var doorGroup(get_doorGroup, set_doorGroup):Int;
}
