package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.Button;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import com.mattarod.labyrinth.gameObjects.scripts.Script;
import com.mattarod.labyrinth.gameObjects.scripts.ScriptRunner;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.tile.FlxTilemap;
import openfl.Assets;

/**
 * A FlxGroup that contains all of the objects in a room.
 *
 * Rooms can be loaded and unloaded by a FlxState by calling
 * add() and remove() on the room.
 *
 * Automatically loads tile data from assets/level/[x],[y].csv
 * Automatically loads scenario data from assets/scenario/[x],[y].csv
 *
 * @author Matthew Rodriguez
 */
class Room extends FlxGroup implements FlxResettableSprite {
	
	// Member variables
	private var _xIndex:Int;
	private var _yIndex:Int;
	
	private var _x:Int;
	private var _y:Int;
	
	private var bg:FlxSprite;
	private var _tilemap:FlxTilemap;
	private var _solids:FlxGroup;
	private var scriptableMemberArray:Array<FlxSprite>;
	
	private var _script:Script;
	private var scriptRunner:ScriptRunner;
	
	private var _paused:Bool;
	
	/**
	 * Create and load a room.
	 *
	 * @param	xIndex The x-index of the room in the dungeon.
	 * @param	yIndex The y-index of the room in the dungeon.
	 */
	public function new(xIndex:Int, yIndex:Int) {
		// Create the FlxGroup.
		super();
		
		// Set the upper-left corner of the room in world coordinates.
		_x = xIndex * KagiNoMeiro.GAME_WIDTH;
		_y = yIndex * KagiNoMeiro.GAME_HEIGHT;
		
		// Set a background image
		bg = new FlxSprite(_x, _y, Assets.getBitmapData("assets/png/bg.png"));
		add(bg);
		
		_tilemap = new FlxTilemap();
		_solids = new FlxGroup();
		scriptableMemberArray = new Array<FlxSprite>();
		
		_script = new Script();
		
		// Set the room coordinates.
		_xIndex = xIndex;
		_yIndex = yIndex;
		
		// Load the room's tile map.
		_tilemap.loadMap(Assets.getText("assets/level/" + _xIndex + "," + _yIndex + ".csv"),
			Assets.getBitmapData("assets/png/autotiles.png"), 
			KagiNoMeiro.TILE_LENGTH, KagiNoMeiro.TILE_LENGTH, 1);
		
		_tilemap.x = _x;
		_tilemap.y = _y;
		
		add(_tilemap);
		
		// Load the room's scenario data.
		loadScenarioData();
		
		_solids.add(_tilemap);
		
		// Begin running the room script.
		scriptRunner = new ScriptRunner(_script);
		
		_paused = true;
	}
	
	/**
	 * Reset the room's scenario data.
	 * Reset every resettable member, and reset the room script.
	 */
	public function resetScenario():Void {
		// Reset every resettable member.
		for (member in scriptableMemberArray) {
			if (Std.is(member, FlxResettableSprite)) {
				var resettable:FlxResettableSprite = cast member;
				resettable.resetScenario();
			}
		}
		
		// Reset the room script.
		_script.reset();
		scriptRunner = new ScriptRunner(_script);
	}
	
	/**
	 * Update function.
	 */
	public override function update():Void {
		if (_paused) return;
		
		// Continue executing the room script.
		scriptRunner.update();
		
		super.update();
	}
	
	/**
	 * Load the room's scenario data.
	 */
	public function loadScenarioData():Void {
		// Load text from file.
		var scenarioDataArrayRaw:String = Assets.getText("assets/scenario/" + _xIndex + "," + _yIndex + ".csv");
		
		// Split on newline.
		var scenarioDataArray:Array<String> = scenarioDataArrayRaw.split("\r\n");
		
		// Parse data from each line.
		for (scenarioDataRaw in scenarioDataArray) {
			// Skip blank lines.
			if (scenarioDataRaw == "") {
				continue;
			}
			
			// Split line by comma.
			var scenarioData:Array<String> = scenarioDataRaw.split(",");
			
			var member:FlxSprite = null;
			
			// Read the first token on the line.
			switch(scenarioData[0]) {
				// key,x,y
				// Place a key at tile x,y in the room.
				case "key":
					if (scenarioData.length != 3) {
						throw("Invalid key parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					
					member = new Key(x, y);
					add(member);
					
				// door,x,y
				// Place a dummy door at tile x,y in the room.
				// door,x,y,b
				// Place a brotherdoor with brotherIndex b at tile x,y in the room.
				// Record it in the Registry for later.
				case "door":
					if (scenarioData.length != 3 && scenarioData.length != 4) {
						throw("Invalid door parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					
					var brotherIndex:Int = -1;
					
					if(scenarioData.length == 4) {
						brotherIndex = Std.parseInt(scenarioData[3]);
					}
					
					var door:Door = new Door(x, y, brotherIndex);
					member = door;
					add(member);
					
					if (brotherIndex != -1) {
						Registry.brotherDoorListSet.set(door);
					}
					
				// book,x,y,t
				// Place a book at tile x,y
				// with text from assets/text/[t].txt in the room
				// book,x,y,t,n
				// Place a book at tile x,y
				// with text from assets/text/[t].txt in the room
				// With sprite "assets/png/[name]book.png"
				case "book":
					if (scenarioData.length != 4 && scenarioData.length != 5) {
						throw("Invalid book parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					var t:String = scenarioData[3];
					
					var name:String = "";
					
					if (scenarioData.length == 5) {
						name = scenarioData[4];
					}
					
					member = new Book(x, y, t, name);
					add(member);
					
				// brotherbook,x,y
				// Place a brotherbook at tile x,y in the room.
				// Record it in the Registry for later.
				case "brotherbook":
					if (scenarioData.length != 3) {
						throw("Invalid brotherbook parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					
					var book:Book = new Book(x, y, null, "brother");
					book.append(Registry.brotherIntros[Registry.brotherBookArray.length] + "\n");
					
					member = book;
					add(member);
					
					Registry.brotherBookArray.push(book);
					
				// generalbook,x,y
				// Place a generalbook at tile x,y in the room.
				// Record it in the Registry for later.
				case "generalbook":
					if (scenarioData.length != 3) {
						throw("Invalid generalbook parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					
					var book:Book = new Book(x, y, null, "brother");
					member = book;
					add(member);
					
					Registry.generalBookArray.push(book);
					
				// lock,s,x0,y0,x1,y1,...
				// Place a lock in the room with speed s, that follows the given path.
				case "lock":
					if (scenarioData.length < 4) {
						throw("Invalid lock parameters: " + scenarioDataRaw);
					}
					
					var path:Array<String> = scenarioData.slice(2, scenarioData.length);
					var speed:Float = Std.parseFloat(scenarioData[1]);
					
					member = new Lock(path, speed, this);
					add(member);
					
				// plat,s,w,h,x0,y0,x1,y1,...
				// Place a platform in the room with speed s,
				// that is w tiles wide and h tiles tall,
				// that follows the given path.
				case "plat":
					if (scenarioData.length < 6) {
						throw("Invalid platform parameters: " + scenarioDataRaw);
					}
					
					var path:Array<String> = scenarioData.slice(4, scenarioData.length);
					var speed:Float = Std.parseFloat(scenarioData[1]);
					var w:Int = Std.parseInt(scenarioData[2]);
					var h:Int = Std.parseInt(scenarioData[3]);
					
					member = new MovingPlatform(path, speed, w, h, this);
					_solids.add(member);
					add(member);
					
				// button,x,y,color,onPress,onRelease,buttontype
				// Place a button at tile x,y in the room.
				// Its sprite is assets/png/[name]button.png.
				// It runs assets/scripts/[onPress].csv when pressed.
				// It runs assets/scripts/[onRelease].csv when released.
				// Its [stickiness] is "sticky" or "notsticky".
				// Its [kind] is "floor" or "area", default "floor".
				// If it is an area button, its width in pixels is [width].
				// If it is an area button, its height in pixels is [height].
				case "button":
					if (scenarioData.length != 7 && scenarioData.length != 8 && scenarioData.length != 10) {
						throw("Invalid button parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					var name:String = scenarioData[3];
					var onPress:String = scenarioData[4];
					var onRelease:String = scenarioData[5];
					var stickiness:String = scenarioData[6];
					
					var kind:String = "floor";
					var width:Int = KagiNoMeiro.TILE_LENGTH;
					var height:Int = KagiNoMeiro.TILE_LENGTH;
					
					if (scenarioData.length == 8) {
						kind = scenarioData[7];
					}
					
					if (scenarioData.length == 10) {
						kind = scenarioData[7];
						width = Std.parseInt(scenarioData[8]);
						height = Std.parseInt(scenarioData[9]);
					}
					
					member = new Button(this, x, y, name, onPress, onRelease, stickiness, kind, width, height);
					add(member);
					
				// block,x,y,name
				// Place a block at tile x,y
				// with sprite assets/text/[name]block.txt in the room
				case "block":
					if (scenarioData.length != 4) {
						throw("Invalid block parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					var name:String = scenarioData[3];
					
					member = new Block(x, y, name);
					_solids.add(member);
					add(member);
					
				// decoration,x,y,name
				// Place a decoration at tile x,y
				// with sprite assets/text/[name].txt in the room
				case "decoration":
					if (scenarioData.length != 4) {
						throw("Invalid decoration parameters: " + scenarioDataRaw);
					}
					
					var x:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[1])) + this.x;
					var y:Float = (KagiNoMeiro.TILE_LENGTH * Std.parseFloat(scenarioData[2])) + this.y;
					var name:String = scenarioData[3];
					
					member = new Decoration(x, y, name);
					add(member);
					
				// script,name
				// Add script assets/script/[name].csv to the room,
				// to execute automatically upon first room entry,
				// and on first entry after room reset
				case "script":
					if (scenarioData.length != 2) {
						throw("Invalid script parameters: " + scenarioDataRaw);
					}
					
					if (!_script.isEmpty()) {
						throw("Error: Room can only have one room script.");
					}
					
					_script.parse(scenarioData[1], this);
					
				// comment,...
				// Ignore comments.
				case "comment":
					continue;
					
				// If the first word isn't recognized, throw an error message.
				default:
					throw("Invalid scenario data: " + scenarioDataRaw);
			}
			
			// Add the member to scriptableMemberArray
			if (member != null) {
				scriptableMemberArray.push(member);
			}
		}
	}
	
	/**
	 * Get a FlxActivatableSprite object at the given index.
	 * Null if the object at the given index isn't FlxActivatableSprite.
	 *
	 * @param	index The index of the FlxActivatableSprite object.
	 * @return The FlxActivatableSprite object at the given index.
	 */
	public function getActivatable(index:Int):FlxActivatableSprite {
		var result:FlxSprite = getFlxSprite(index);
		
		if (!Std.is(result, FlxActivatableSprite)) {
			throw("Object at index is not activatable: " + index);
			return null;
		}
		
		return cast result;
	}
	
	/**
	 * Get the FlxSprite at the given index.
	 * Null if out of bounds.
	 *
	 * @param	index The index of the FlxSprite.
	 * @return The FlxSprite at the given index.
	 */
	public function getFlxSprite(index:Int):FlxSprite {
		var result:FlxSprite = scriptableMemberArray[index];
		
		if (result == null) {
			throw("Object at index does not exist: " + index);
			return null;
		}
		
		return result;
	}
	
	// Getters and setters
	function get_xIndex():Int {return _xIndex;}
	public var xIndex(get_xIndex, null):Int;
	function get_yIndex():Int {return _yIndex;}
	public var yIndex(get_yIndex, null):Int;
	function get_x():Int {return _x;}
	public var x(get_x, null):Int;
	function get_y():Int {return _y;}
	public var y(get_y, null):Int;
	function get_tilemap():FlxTilemap {return _tilemap;}
	public var tilemap(get_tilemap, null):FlxTilemap;
	function get_solids():FlxGroup {return _solids;}
	public var solids(get_solids, null):FlxGroup;
	function get_paused():Bool {return _paused;}
	function set_paused(value:Bool):Bool {return _paused = value;}
	public var paused(get_paused, set_paused):Bool;
}
