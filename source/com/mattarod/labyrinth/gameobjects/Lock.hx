package com.mattarod.labyrinth.gameobjects;

import com.mattarod.labyrinth.gameobjects.interfaces.FlxActivatableSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxInteractiveSprite;
import com.mattarod.labyrinth.gameobjects.interfaces.FlxResettableSprite;
import com.mattarod.labyrinth.KagiNoMeiro;
import com.mattarod.labyrinth.Registry;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import openfl.Assets;

/**
 * An in-game object representing an enemy lock.
 *
 * @author Matthew Rodriguez
 */
class Lock extends FlxSprite implements FlxInteractiveSprite implements FlxActivatableSprite implements FlxResettableSprite {
	
	/**
	 * The width of the lock hitbox.
	 */
	public static inline var WIDTH:Float = 10;
	
	/**
	 * The height of the lock hitbox.
	 */
	public static inline var HEIGHT:Float = 10;
	
	/**
	 * The x-offset of the sprite.
	 */
	public static inline var X_OFFSET:Float = 3;
	
	/**
	 * The y-offset of the sprite.
	 */
	public static inline var Y_OFFSET:Float = 3;
	
	private var path:Array<FlxPoint>;
	private var iteration:Int;
	private var speed:Float;
	private var timer:Float;
	
	private var switchActive:Bool;
	private var moving = true;
	
	/**
	 * Create a new Lock.
	 *
	 * @param	rawPath The path that the lock will follow as a flat array.
	 * @param	speed The speed of the lock in positions per second.
	 * @param	room The room this lock is in.
	 */
	public function new(rawPath:Array<String>, speed:Float = 1, room:Room) {
		super();
		
		this.speed = speed;
		
		immovable = true;
		
		// Convert path from array of strings to array of FlxPoints.
		path = new Array<FlxPoint>();
		
		for (i in 0...Math.floor(rawPath.length/2)) {
			var stepX:Float = Std.parseFloat(rawPath[2 * i]);
			var stepY:Float = Std.parseFloat(rawPath[2 * i + 1]);
			
			path.push(new FlxPoint(
				stepX * KagiNoMeiro.TILE_LENGTH + X_OFFSET + room.x,
				stepY * KagiNoMeiro.TILE_LENGTH + Y_OFFSET + room.y
			));
		}
		
		// Initialize all resettable fields.
		resetScenario();
	}
	
	/**
	 * Reset this lock to its original conditions.
	 */
	public function resetScenario():Void {
		// Load the graphics
		loadGraphic(Assets.getBitmapData("assets/png/lock.png"));
		
		// Set dimensions
		x = path[0].x;
		y = path[0].y;
		
		height = HEIGHT;
		width = WIDTH;
		
		offset.x = X_OFFSET;
		offset.y = Y_OFFSET;
		
		// Reset to beginning of path
		iteration = path.length - 1;
		timer = 0;
		
		switchActive = true;
		visible = true;
		moving = false;
		
		velocity.x = 0;
		velocity.y = 0;
	}
	
	/**
	 * Called when the player collides with this lock.
	 * Take away the key if the player has it.
	 */
	public function onPlayerCollision():Void {
		if (Registry.gameState.player.hasKey) {
			// Take away the key.
			Registry.gameState.player.hasKey = false;
			
			// Put the key back on the first screen.
			Registry.key.exists = true;
			
			// Play the animation of the lock taking the key away.
			visible = false;
			FlxG.sound.play("LoseKey");
			Registry.lockThatTookKey = this;
			Registry.gameState.playKeyLossAnimation();
		}
	}
	
	/**
	 * Update function.
	 */
	public override function update():Void {
		super.update();
		
		// If this object is turned off,
		// and not currently moving between points in the path,
		// do nothing.
		if (!switchActive && !moving) {
			return;
		}
		
		// Determine which step of the path this object is on,
		// based on the timer and the speed.
		var iterationThisFrame:Int = Math.floor(timer * speed) % path.length;
		
		// What to do when the iteration increments.
		if (iterationThisFrame != iteration) {
			
			// Snap to the next position on the list,
			// to prevent drifting.
			var snapToIndex = iterationThisFrame;
			x = path[snapToIndex].x;
			y = path[snapToIndex].y;
			
			// If the switch is off, stop moving.
			if (moving && !switchActive) {
				moving = false;
				velocity.x = 0;
				velocity.y = 0;
				return;
				
			} 
			
			moving = true;
			iteration = iterationThisFrame;
			
			// Determine the next destination.
			var destinationIndex:Int = (iterationThisFrame+1) % path.length;
			var destination:FlxPoint = path[destinationIndex];
			
			// Set the velocity so that the platform reaches the destination
			// at the end of this iteration.
			velocity.x = (destination.x - x) * speed;
			velocity.y = (destination.y - y) * speed;
		}
		
		// Update the timer if the object is moving.
		if(moving) timer += FlxG.elapsed;
	}
	
	/**
	 * Set whether the platform is moving.
	 *
	 * @param	b
	 */
	public function setSwitchActive(b:Bool):Void {
		switchActive = b;
	}
	
	/**
	 * Toggle whether the platform is moving.
	 */
	public function toggleSwitch():Void {
		switchActive = !switchActive;
	}
}
