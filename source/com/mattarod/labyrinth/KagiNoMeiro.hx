package com.mattarod.labyrinth;

import com.mattarod.labyrinth.gameobjects.Book;
import com.mattarod.labyrinth.gameobjects.Door;
import com.mattarod.labyrinth.gameobjects.Room;
import com.mattarod.labyrinth.misc.Misc;
import com.mattarod.labyrinth.states.TitleScreen;
import de.polygonal.ds.ListSet.ListSet;
import flash.events.Event;
import flash.Lib;
import flixel.FlxG;
import flixel.FlxGame;
import openfl.Assets;

class KagiNoMeiro extends FlxGame {
	/**
	 * How tall the game is, in game pixels.
	 */
	public static inline var GAME_HEIGHT:Int = 240;
	
	/**
	 * How wide the game is, in game pixels.
	 */
	public static inline var GAME_WIDTH:Int = 320;
	
	/**
	 * The length of a tile in pixels.
	 */
	public static inline var TILE_LENGTH:Int = 16;
	
	/**
	 * The volume of the music.
	 */
	public static inline var MUSIC_VOLUME:Float = 0.25;
	
	private var _xOffset:Int = 0;
	private var _yOffset:Int = 0;
	private var _ratio:Int = 2;
	
	private var doorGroups:Array<Array<Door>>;
	private var startRoomX:Int;
	private var startRoomY:Int;
	private var brotherDoorArray:Array<Door>;
	
	/**
	 * Create the game.
	 */
	public function new() {
		Registry.game = this;
		
		// Set the background to black.
		Lib.current.stage.color = 0xFF000000;
		
		// Figure out the dimensions of the stage.
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;
		var fps:Int = 60;
		
		// Determine the stage-pixel/game-pixel ratio.
		var ratioX:Float = stageWidth / GAME_WIDTH;
		var ratioY:Float = stageHeight / GAME_HEIGHT;
		_ratio = Math.floor(Math.min(ratioX, ratioY));
		
		super(GAME_WIDTH, GAME_HEIGHT, TitleScreen, _ratio, fps, fps);
		
		// Load the game data from files.
		setUp();
	}
	
	/**
	 * Load the game data from files.
	 */
	public function setUp() {
		// Parse config.ini
		parseConfigFile();
		
		// Load the dungeon.
		loadDungeon();
	}
	
	/**
	 * Readjust the camera when the stage is resized.
	 * @param	E
	 */
	public override function onResize(E:Event = null):Void {
		super.onResize(E);
		
		readjustCamera();
	}
	
	/**
	 * Figure out the ideal settings for the camera.
	 */
	public function readjustCamera():Void {
		// Figure out the dimensions of the stage.
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;
		
		// Determine the ratio of stage-pixels to game-pixels.
		var ratioX:Float = stageWidth / GAME_WIDTH;
		var ratioY:Float = stageHeight / GAME_HEIGHT;
		_ratio = Math.floor(Math.min(ratioX, ratioY));
		
		// Determine the position to place a camera to center it.
		_xOffset = Math.floor((stageWidth - (_ratio * GAME_WIDTH)) / 2);
		_yOffset = Math.floor((stageHeight - (_ratio * GAME_HEIGHT)) / 2);
		
		// Center all cameras.
		for (camera in FlxG.cameras.list) {
			camera.zoom = _ratio;
			camera.x = _xOffset;
			camera.y = _yOffset;
		}
	}
	
	/**
	 * Parse config.ini.
	 * It's just a file full of keybinds.
	 * This method does have some fault-tolerance for improper config files,
	 * but it's not extensive. It definitely follows a GIGO policy.
	 */
	public function parseConfigFile():Void {
		var configFileRaw:String = Assets.getText("config.ini");
		
		var configFile:Array<String> = configFileRaw.split("\r\n");
		
		for (lineRaw in configFile) {
			var line:Array<String> = lineRaw.split("=");
			if (line.length != 2) {
				continue;
			}
			
			switch(line[0]) {
				case "move_left":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.moveLeft = line[1];
					}
					
				case "move_right":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.moveRight = line[1];
					}
					
				case "jump":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.jump = line[1];
					}
					
				case "use_key":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.useKey = line[1];
					}
					
				case "warp":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.warp = line[1];
					}
					
				case "reset":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.reset = line[1];
					}
					
				case "pause":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.pause = line[1];
					}
					
				case "mute_music":
					if (FlxG.keyboard.getKeyCode(line[1]) != null) {
						Config.muteMusic = line[1];
					}
			}
		}
	}
	
	/**
	 * Load all of the game data.
	 */
	public function loadDungeon():Void {
		
		// Initialize some global collections
		Registry.brotherDoorListSet = new ListSet<Door>();
		Registry.brotherBookArray = new Array<Book>();
		Registry.generalBookArray = new Array<Book>();
		Registry.rooms = new Array<Array<Room>>();
		
		// Load the strings that will start brotherbooks
		var brotherIntrosRaw:String = Assets.getText("assets/brother/brothers.csv");
		Registry.brotherIntros = brotherIntrosRaw.split("\r\n");
		
		// Load the meta file.
		loadMetaFile();
		
		// Load the rooms of the dungeon.
		for (x in 0...Registry.roomsWide) {
			Registry.rooms[x] = new Array<Room>();
			
			for (y in 0...Registry.roomsTall) {
				var room:Room = new Room(x, y);
				
				Registry.rooms[x].push(room);
			}
		}
		
		// Set the start room.
		Registry.startRoom = Registry.rooms[startRoomX][startRoomY];
		
		// Set up the puzzle that points to the exit door.
		populatePuzzle();
		
		// Load the rooms that must be reset when the key is picked up.
		loadResetRooms();
	}
	
	/**
	 * Load important global values.
	 */
	public function loadMetaFile():Void {
		
		// Load the text of the meta file.
		var metaRaw:String = Assets.getText("assets/level/meta.csv");
		
		// Split by newline.
		var metaArray:Array<String> = metaRaw.split("\r\n");
		
		// Load dungeon dimensions.
		var dungeonDimensionsRaw:String = metaArray[0];
		var dungeonDimensions:Array<String> = dungeonDimensionsRaw.split(",");
		
		Registry.roomsWide = Std.parseInt(dungeonDimensions[0]);
		Registry.roomsTall = Std.parseInt(dungeonDimensions[1]);
		
		// Load start room.
		var startRoomRaw:String = metaArray[1];
		var startRoom:Array<String> = startRoomRaw.split(",");
		
		startRoomX = Std.parseInt(startRoom[0]);
		startRoomY = Std.parseInt(startRoom[1]);
		
		// Load player start position.
		var startPositionRaw:String = metaArray[2];
		var startPosition:Array<String> = startPositionRaw.split(",");
		
		Registry.startPositionX = TILE_LENGTH * Std.parseFloat(startPosition[0]);
		Registry.startPositionY = TILE_LENGTH * Std.parseFloat(startPosition[1]);
		
		// Load number of flags
		Registry.flags = new Array<Bool>();
		for (i in 0...Std.parseInt(metaArray[3])) {
			Registry.flags.push(false);
		}
	}
	
	/**
	 * Create the brotherDoorArray, indexed by brother door index,
	 * using the Registry's brotherDoorListSet.
	 */
	public function makeBrotherDoorArray():Void {
		
		brotherDoorArray = new Array<Door>();
		
		// Push n nulls into the array.
		for (i in 0...Registry.brotherDoorListSet.size()) {
			brotherDoorArray.push(null);
		}
		
		// Put each brotherDoor into the array at its index.
		for (door in Registry.brotherDoorListSet) {
			// Check for erroneous game data.
			if (brotherDoorArray[door.brotherDoorIndex] != null) {
				throw "More than one door assigned brotherdoor index: " + door.brotherDoorIndex;
			}
			
			if (door.brotherDoorIndex >= brotherDoorArray.length) {
				throw "Brother door assigned to index " + door.brotherDoorIndex + ", but only " +
					brotherDoorArray.length + "brother doors declared.";
			}
			
			brotherDoorArray[door.brotherDoorIndex] = door;
		}
	}
	
	/**
	 * Set up the pieces for the puzzle to determine the exit door.
	 */
	public function populatePuzzle():Void {
		
		makeBrotherDoorArray();
		
		// Get local refs to Registry vars, for code compactness
		var brotherDoorListSet:ListSet<Door> = Registry.brotherDoorListSet;
		var brotherBookArray:Array<Book> = Registry.brotherBookArray;
		var generalBookArray:Array<Book> = Registry.generalBookArray;
		
		// Calculate the numbers involved
		var numberOfBrotherDoors:Int = brotherDoorListSet.size();
		var numberOfBrothers:Int = brotherBookArray.length;
		var numberOfGeneralBooksRequired:Int = Misc.binaryLogCeiling(numberOfBrothers);
		var numberOfGeneralBooks:Int = generalBookArray.length;
		
		// Throw an error if the puzzle requires a random guess to solve
		if (numberOfGeneralBooksRequired > numberOfGeneralBooks) {
			throw ("Not enough General Hint books to solve puzzle.");
		}
		
		// Load doorgroups.csv
		loadDoorGroups();
		
		// Randomly choose the winning door
		var exitDoorIndex:Int = Std.random(numberOfBrotherDoors);
		var exitDoor:Door = brotherDoorArray[exitDoorIndex];
		var winningBrother:Int = exitDoor.doorGroup;
		exitDoor.isExit = true;
		var winningBook:Book = brotherBookArray[winningBrother];
		
		// Assign text to the winning brother book
		winningBook.appendFile("brother" + exitDoor.brotherDoorIndex);
		
		// Randomly assign each of the other brother books
		// to a random door in that brother's group
		for (i in 0...numberOfBrothers) {
			if (i != exitDoor.doorGroup) {
				var doorGroup:Array<Door> = doorGroups[i];
				var doorIndex:Int = Std.random(doorGroup.length);
				var door:Door = doorGroup[doorIndex];
				var book:Book = brotherBookArray[i];
				
				// Assign text to the brother book
				book.appendFile("brother" + door.brotherDoorIndex);
				
				// Mark losing brotherbooks read as reading them is not
				// necessary to solve the puzzle
				book.read = true;
			}
		}
		
		// Load the winning door's group traits
		var traits:Array<Bool> = loadDoorGroupTraits(numberOfGeneralBooks, winningBrother);
		
		// Populate the generalbooks with the winning door's group traits
		for (i in 0...traits.length) {
			var trait:Bool = traits[i];
			
			var book:Book = generalBookArray[i];
			
			if (book == null) break;
			
			book.appendFile("trait" + i + trait);
		}
	}
	
	/**
	 * Parse doorgroups.csv,
	 * and sort the brotherdoors into doorGroups
	 */
	public function loadDoorGroups():Void {
		
		// Initialize the doorGroups 2D array
		doorGroups = new Array<Array<Door>>();
		
		for (i in 0...Registry.brotherBookArray.length) {
			doorGroups.push(new Array<Door>());
		}
		
		// parse doorgroups.csv
		var doorGroupsRaw:String = Assets.getText("assets/brother/doorgroups.csv");
		var doorGroupIndexes:Array<String> = doorGroupsRaw.split("\r\n");
		
		for (i in 0...doorGroupIndexes.length) {
			// Line i indicates which doorGroup that door i belongs to
			
			if (doorGroupIndexes[i] == "") continue;
			
			var doorGroupIndex:Int = Std.parseInt(doorGroupIndexes[i]);
			var doorGroup:Array<Door> = doorGroups[doorGroupIndex];
			var door = brotherDoorArray[i];
			
			door.doorGroup = doorGroupIndex;
			doorGroup.push(door);
		}
	}
	
	/**
	 * Load the traits for the given doorGroup.
	 *
	 * @param	numberOfTraits The number of traits expected.
	 * @param	index The doorGroup to load the traits for.
	 * @return The traits of doorGroup[index]
	 */
	public function loadDoorGroupTraits(numberOfTraits:Int, index:Int):Array<Bool> {
		
		var groupTraitsRaw:String = Assets.getText("assets/brother/grouptraits.csv");
		var groupTraits:Array<String> = groupTraitsRaw.split("\r\n");
		var groupData:String = groupTraits[index];
		var params:Array<String> = groupData.split(",");
		
		if (params.length != numberOfTraits) {
			throw("invalid grouptrait data: " + groupData);
		}
		
		return Misc.parseBoolArray(params);
	}
	
	/**
	 * Load the file containing the rooms that need to be reset
	 * when the key is picked up.
	 */
	public function loadResetRooms():Void {
		
		Registry.resetRooms = new ListSet<Room>();
		
		// parse reset.csv
		var resetRoomsRaw:String = Assets.getText("assets/level/reset.csv");
		var resetRoomsArray:Array<String> = resetRoomsRaw.split("\r\n");
		
		// Add the room to the array.
		for (resetRoom in resetRoomsArray) {
			if (resetRoom == "") continue;
			
			var params:Array<String> = resetRoom.split(",");
			
			var x:Int = Std.parseInt(params[0]);
			var y:Int = Std.parseInt(params[1]);
			
			Registry.resetRooms.set(Registry.rooms[x][y]);
		}
	}
	
	// Getters
	function get_xOffset():Int 
	{return _xOffset;}
	public var xOffset(get_xOffset, null):Int;
	function get_yOffset():Int 
	{return _yOffset;}
	public var yOffset(get_yOffset, null):Int;
	function get_ratio():Int {return _ratio;}
	public var ratio(get_ratio, null):Int;
}
